<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210826100903 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE ability_text_block');
        $this->addSql('DROP TABLE arc_text_block');
        $this->addSql('DROP TABLE character_text_block');
        $this->addSql('DROP TABLE episode_text_block');
        $this->addSql('DROP TABLE organization_text_block');
        $this->addSql('DROP TABLE volume_text_block');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ability_text_block (id INT AUTO_INCREMENT NOT NULL, ability_id INT DEFAULT NULL, spoil_chapter_id INT DEFAULT NULL, title VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, media VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_17ECA89E3D6D5A0E (spoil_chapter_id), INDEX IDX_17ECA89E8016D8B2 (ability_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE arc_text_block (id INT AUTO_INCREMENT NOT NULL, arc_id INT NOT NULL, spoil_chapter_id INT DEFAULT NULL, title VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, media VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_986E0C443D6D5A0E (spoil_chapter_id), INDEX IDX_986E0C4441EB8A3C (arc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE character_text_block (id INT AUTO_INCREMENT NOT NULL, character_obj_id INT NOT NULL, spoil_chapter_id INT DEFAULT NULL, title VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, media VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_642394F93D6D5A0E (spoil_chapter_id), INDEX IDX_642394F9C54BF0D7 (character_obj_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE episode_text_block (id INT AUTO_INCREMENT NOT NULL, episode_id INT NOT NULL, spoil_chapter_id INT DEFAULT NULL, title VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, media VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_8A1467E13D6D5A0E (spoil_chapter_id), INDEX IDX_8A1467E1362B62A0 (episode_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE organization_text_block (id INT AUTO_INCREMENT NOT NULL, organization_id INT NOT NULL, spoil_chapter_id INT DEFAULT NULL, title VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, media VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_3CF4BF903D6D5A0E (spoil_chapter_id), INDEX IDX_3CF4BF9032C8A3DE (organization_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE volume_text_block (id INT AUTO_INCREMENT NOT NULL, volume_id INT NOT NULL, spoil_chapter_id INT DEFAULT NULL, title VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, media VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_F08E9E873D6D5A0E (spoil_chapter_id), INDEX IDX_F08E9E878FD80EEA (volume_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE ability_text_block ADD CONSTRAINT FK_17ECA89E3D6D5A0E FOREIGN KEY (spoil_chapter_id) REFERENCES chapter (id)');
        $this->addSql('ALTER TABLE ability_text_block ADD CONSTRAINT FK_17ECA89E8016D8B2 FOREIGN KEY (ability_id) REFERENCES ability (id)');
        $this->addSql('ALTER TABLE arc_text_block ADD CONSTRAINT FK_986E0C443D6D5A0E FOREIGN KEY (spoil_chapter_id) REFERENCES chapter (id)');
        $this->addSql('ALTER TABLE arc_text_block ADD CONSTRAINT FK_986E0C4441EB8A3C FOREIGN KEY (arc_id) REFERENCES arc (id)');
        $this->addSql('ALTER TABLE character_text_block ADD CONSTRAINT FK_642394F93D6D5A0E FOREIGN KEY (spoil_chapter_id) REFERENCES chapter (id)');
        $this->addSql('ALTER TABLE character_text_block ADD CONSTRAINT FK_642394F9C54BF0D7 FOREIGN KEY (character_obj_id) REFERENCES `character` (id)');
        $this->addSql('ALTER TABLE episode_text_block ADD CONSTRAINT FK_8A1467E1362B62A0 FOREIGN KEY (episode_id) REFERENCES episode (id)');
        $this->addSql('ALTER TABLE episode_text_block ADD CONSTRAINT FK_8A1467E13D6D5A0E FOREIGN KEY (spoil_chapter_id) REFERENCES chapter (id)');
        $this->addSql('ALTER TABLE organization_text_block ADD CONSTRAINT FK_3CF4BF9032C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id)');
        $this->addSql('ALTER TABLE organization_text_block ADD CONSTRAINT FK_3CF4BF903D6D5A0E FOREIGN KEY (spoil_chapter_id) REFERENCES chapter (id)');
        $this->addSql('ALTER TABLE volume_text_block ADD CONSTRAINT FK_F08E9E873D6D5A0E FOREIGN KEY (spoil_chapter_id) REFERENCES chapter (id)');
        $this->addSql('ALTER TABLE volume_text_block ADD CONSTRAINT FK_F08E9E878FD80EEA FOREIGN KEY (volume_id) REFERENCES volume (id)');
    }
}
