<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210908123811 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE episode DROP FOREIGN KEY FK_DDAA1CDAE682647F');
        $this->addSql('DROP INDEX IDX_DDAA1CDAE682647F ON episode');
        $this->addSql('ALTER TABLE episode DROP soundtracks_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE episode ADD soundtracks_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE episode ADD CONSTRAINT FK_DDAA1CDAE682647F FOREIGN KEY (soundtracks_id) REFERENCES soundtrack (id)');
        $this->addSql('CREATE INDEX IDX_DDAA1CDAE682647F ON episode (soundtracks_id)');
    }
}
