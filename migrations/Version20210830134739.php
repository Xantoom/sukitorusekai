<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210830134739 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE character_media (character_id INT NOT NULL, media_id INT NOT NULL, INDEX IDX_94EF466E1136BE75 (character_id), INDEX IDX_94EF466EEA9FDD75 (media_id), PRIMARY KEY(character_id, media_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE episode_media (episode_id INT NOT NULL, media_id INT NOT NULL, INDEX IDX_51265758362B62A0 (episode_id), INDEX IDX_51265758EA9FDD75 (media_id), PRIMARY KEY(episode_id, media_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE character_media ADD CONSTRAINT FK_94EF466E1136BE75 FOREIGN KEY (character_id) REFERENCES `character` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE character_media ADD CONSTRAINT FK_94EF466EEA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE episode_media ADD CONSTRAINT FK_51265758362B62A0 FOREIGN KEY (episode_id) REFERENCES episode (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE episode_media ADD CONSTRAINT FK_51265758EA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE arc_media');
        $this->addSql('ALTER TABLE arc ADD cover_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE arc ADD CONSTRAINT FK_7FE65393922726E9 FOREIGN KEY (cover_id) REFERENCES media (id)');
        $this->addSql('CREATE INDEX IDX_7FE65393922726E9 ON arc (cover_id)');
        $this->addSql('ALTER TABLE bluray ADD cover_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bluray ADD CONSTRAINT FK_F697EA50922726E9 FOREIGN KEY (cover_id) REFERENCES media (id)');
        $this->addSql('CREATE INDEX IDX_F697EA50922726E9 ON bluray (cover_id)');
        $this->addSql('ALTER TABLE disc ADD cover_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE disc ADD CONSTRAINT FK_2AF5530922726E9 FOREIGN KEY (cover_id) REFERENCES media (id)');
        $this->addSql('CREATE INDEX IDX_2AF5530922726E9 ON disc (cover_id)');
        $this->addSql('ALTER TABLE season ADD cover_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE season ADD CONSTRAINT FK_F0E45BA9922726E9 FOREIGN KEY (cover_id) REFERENCES media (id)');
        $this->addSql('CREATE INDEX IDX_F0E45BA9922726E9 ON season (cover_id)');
        $this->addSql('ALTER TABLE volume ADD cover_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE volume ADD CONSTRAINT FK_B99ACDDE922726E9 FOREIGN KEY (cover_id) REFERENCES media (id)');
        $this->addSql('CREATE INDEX IDX_B99ACDDE922726E9 ON volume (cover_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE arc_media (arc_id INT NOT NULL, media_id INT NOT NULL, INDEX IDX_A8ECADEF41EB8A3C (arc_id), INDEX IDX_A8ECADEFEA9FDD75 (media_id), PRIMARY KEY(arc_id, media_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE arc_media ADD CONSTRAINT FK_A8ECADEF41EB8A3C FOREIGN KEY (arc_id) REFERENCES arc (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE arc_media ADD CONSTRAINT FK_A8ECADEFEA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE character_media');
        $this->addSql('DROP TABLE episode_media');
        $this->addSql('ALTER TABLE arc DROP FOREIGN KEY FK_7FE65393922726E9');
        $this->addSql('DROP INDEX IDX_7FE65393922726E9 ON arc');
        $this->addSql('ALTER TABLE arc DROP cover_id');
        $this->addSql('ALTER TABLE bluray DROP FOREIGN KEY FK_F697EA50922726E9');
        $this->addSql('DROP INDEX IDX_F697EA50922726E9 ON bluray');
        $this->addSql('ALTER TABLE bluray DROP cover_id');
        $this->addSql('ALTER TABLE disc DROP FOREIGN KEY FK_2AF5530922726E9');
        $this->addSql('DROP INDEX IDX_2AF5530922726E9 ON disc');
        $this->addSql('ALTER TABLE disc DROP cover_id');
        $this->addSql('ALTER TABLE season DROP FOREIGN KEY FK_F0E45BA9922726E9');
        $this->addSql('DROP INDEX IDX_F0E45BA9922726E9 ON season');
        $this->addSql('ALTER TABLE season DROP cover_id');
        $this->addSql('ALTER TABLE volume DROP FOREIGN KEY FK_B99ACDDE922726E9');
        $this->addSql('DROP INDEX IDX_B99ACDDE922726E9 ON volume');
        $this->addSql('ALTER TABLE volume DROP cover_id');
    }
}
