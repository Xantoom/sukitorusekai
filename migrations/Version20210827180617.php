<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210827180617 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ability ADD spoil_chapter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ability ADD CONSTRAINT FK_35CFEE3C3D6D5A0E FOREIGN KEY (spoil_chapter_id) REFERENCES chapter (id)');
        $this->addSql('CREATE INDEX IDX_35CFEE3C3D6D5A0E ON ability (spoil_chapter_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ability DROP FOREIGN KEY FK_35CFEE3C3D6D5A0E');
        $this->addSql('DROP INDEX IDX_35CFEE3C3D6D5A0E ON ability');
        $this->addSql('ALTER TABLE ability DROP spoil_chapter_id');
    }
}
