<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211014111703 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE volume DROP FOREIGN KEY FK_B99ACDDE922726E9');
        $this->addSql('DROP INDEX IDX_B99ACDDE922726E9 ON volume');
        $this->addSql('ALTER TABLE volume DROP cover_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE volume ADD cover_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE volume ADD CONSTRAINT FK_B99ACDDE922726E9 FOREIGN KEY (cover_id) REFERENCES media (id)');
        $this->addSql('CREATE INDEX IDX_B99ACDDE922726E9 ON volume (cover_id)');
    }
}
