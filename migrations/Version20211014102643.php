<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211014102643 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bluray_media (bluray_id INT NOT NULL, media_id INT NOT NULL, INDEX IDX_1F204C66C007F6F7 (bluray_id), INDEX IDX_1F204C66EA9FDD75 (media_id), PRIMARY KEY(bluray_id, media_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bluray_media ADD CONSTRAINT FK_1F204C66C007F6F7 FOREIGN KEY (bluray_id) REFERENCES bluray (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE bluray_media ADD CONSTRAINT FK_1F204C66EA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE bluray_media');
    }
}
