<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210818230001 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE volume (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, romaji VARCHAR(255) NOT NULL, released_at DATETIME DEFAULT NULL, media VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE volume_chapter (volume_id INT NOT NULL, chapter_id INT NOT NULL, INDEX IDX_92BE91768FD80EEA (volume_id), INDEX IDX_92BE9176579F4768 (chapter_id), PRIMARY KEY(volume_id, chapter_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE volume_chapter ADD CONSTRAINT FK_92BE91768FD80EEA FOREIGN KEY (volume_id) REFERENCES volume (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE volume_chapter ADD CONSTRAINT FK_92BE9176579F4768 FOREIGN KEY (chapter_id) REFERENCES chapter (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE arc ADD slug VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE volume_chapter DROP FOREIGN KEY FK_92BE91768FD80EEA');
        $this->addSql('DROP TABLE volume');
        $this->addSql('DROP TABLE volume_chapter');
        $this->addSql('ALTER TABLE arc DROP slug');
    }
}
