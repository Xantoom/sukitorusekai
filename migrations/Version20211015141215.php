<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211015141215 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE media_arc (media_id INT NOT NULL, arc_id INT NOT NULL, INDEX IDX_34B7216EEA9FDD75 (media_id), INDEX IDX_34B7216E41EB8A3C (arc_id), PRIMARY KEY(media_id, arc_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media_bluray (media_id INT NOT NULL, bluray_id INT NOT NULL, INDEX IDX_4AEE708EEA9FDD75 (media_id), INDEX IDX_4AEE708EC007F6F7 (bluray_id), PRIMARY KEY(media_id, bluray_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media_volume (media_id INT NOT NULL, volume_id INT NOT NULL, INDEX IDX_5E35700EA9FDD75 (media_id), INDEX IDX_5E357008FD80EEA (volume_id), PRIMARY KEY(media_id, volume_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media_disc (media_id INT NOT NULL, disc_id INT NOT NULL, INDEX IDX_C1E88AE3EA9FDD75 (media_id), INDEX IDX_C1E88AE3C38F37CA (disc_id), PRIMARY KEY(media_id, disc_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media_post (media_id INT NOT NULL, post_id INT NOT NULL, INDEX IDX_99CDB35EEA9FDD75 (media_id), INDEX IDX_99CDB35E4B89032C (post_id), PRIMARY KEY(media_id, post_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media_season (media_id INT NOT NULL, season_id INT NOT NULL, INDEX IDX_4C9DC177EA9FDD75 (media_id), INDEX IDX_4C9DC1774EC001D1 (season_id), PRIMARY KEY(media_id, season_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE media_arc ADD CONSTRAINT FK_34B7216EEA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media_arc ADD CONSTRAINT FK_34B7216E41EB8A3C FOREIGN KEY (arc_id) REFERENCES arc (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media_bluray ADD CONSTRAINT FK_4AEE708EEA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media_bluray ADD CONSTRAINT FK_4AEE708EC007F6F7 FOREIGN KEY (bluray_id) REFERENCES bluray (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media_volume ADD CONSTRAINT FK_5E35700EA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media_volume ADD CONSTRAINT FK_5E357008FD80EEA FOREIGN KEY (volume_id) REFERENCES volume (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media_disc ADD CONSTRAINT FK_C1E88AE3EA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media_disc ADD CONSTRAINT FK_C1E88AE3C38F37CA FOREIGN KEY (disc_id) REFERENCES disc (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media_post ADD CONSTRAINT FK_99CDB35EEA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media_post ADD CONSTRAINT FK_99CDB35E4B89032C FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media_season ADD CONSTRAINT FK_4C9DC177EA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media_season ADD CONSTRAINT FK_4C9DC1774EC001D1 FOREIGN KEY (season_id) REFERENCES season (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE media_arc');
        $this->addSql('DROP TABLE media_bluray');
        $this->addSql('DROP TABLE media_volume');
        $this->addSql('DROP TABLE media_disc');
        $this->addSql('DROP TABLE media_post');
        $this->addSql('DROP TABLE media_season');
    }
}
