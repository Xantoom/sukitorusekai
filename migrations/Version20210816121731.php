<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210816121731 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64988248E1A');
        $this->addSql('DROP INDEX IDX_8D93D64988248E1A ON user');
        $this->addSql('ALTER TABLE user DROP current_chapter_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD current_chapter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64988248E1A FOREIGN KEY (current_chapter_id) REFERENCES chapter (id)');
        $this->addSql('CREATE INDEX IDX_8D93D64988248E1A ON user (current_chapter_id)');
    }
}
