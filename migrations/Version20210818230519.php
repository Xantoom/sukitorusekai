<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210818230519 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE volume_text_block ADD spoil_chapter_id INT DEFAULT NULL, ADD title VARCHAR(255) DEFAULT NULL, ADD content LONGTEXT NOT NULL, ADD media VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE volume_text_block ADD CONSTRAINT FK_F08E9E873D6D5A0E FOREIGN KEY (spoil_chapter_id) REFERENCES chapter (id)');
        $this->addSql('CREATE INDEX IDX_F08E9E873D6D5A0E ON volume_text_block (spoil_chapter_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE volume_text_block DROP FOREIGN KEY FK_F08E9E873D6D5A0E');
        $this->addSql('DROP INDEX IDX_F08E9E873D6D5A0E ON volume_text_block');
        $this->addSql('ALTER TABLE volume_text_block DROP spoil_chapter_id, DROP title, DROP content, DROP media');
    }
}
