<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210818113315 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE text_block');
        $this->addSql('ALTER TABLE ability_text_block ADD spoil_chapter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ability_text_block ADD CONSTRAINT FK_17ECA89E3D6D5A0E FOREIGN KEY (spoil_chapter_id) REFERENCES chapter (id)');
        $this->addSql('CREATE INDEX IDX_17ECA89E3D6D5A0E ON ability_text_block (spoil_chapter_id)');
        $this->addSql('ALTER TABLE arc_text_block ADD spoil_chapter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE arc_text_block ADD CONSTRAINT FK_986E0C443D6D5A0E FOREIGN KEY (spoil_chapter_id) REFERENCES chapter (id)');
        $this->addSql('CREATE INDEX IDX_986E0C443D6D5A0E ON arc_text_block (spoil_chapter_id)');
        $this->addSql('ALTER TABLE character_text_block ADD spoil_chapter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE character_text_block ADD CONSTRAINT FK_642394F93D6D5A0E FOREIGN KEY (spoil_chapter_id) REFERENCES chapter (id)');
        $this->addSql('CREATE INDEX IDX_642394F93D6D5A0E ON character_text_block (spoil_chapter_id)');
        $this->addSql('ALTER TABLE episode_text_block ADD spoil_chapter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE episode_text_block ADD CONSTRAINT FK_8A1467E13D6D5A0E FOREIGN KEY (spoil_chapter_id) REFERENCES chapter (id)');
        $this->addSql('CREATE INDEX IDX_8A1467E13D6D5A0E ON episode_text_block (spoil_chapter_id)');
        $this->addSql('ALTER TABLE organization_text_block ADD spoil_chapter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE organization_text_block ADD CONSTRAINT FK_3CF4BF903D6D5A0E FOREIGN KEY (spoil_chapter_id) REFERENCES chapter (id)');
        $this->addSql('CREATE INDEX IDX_3CF4BF903D6D5A0E ON organization_text_block (spoil_chapter_id)');
        $this->addSql('ALTER TABLE post_text_block ADD spoil_chapter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE post_text_block ADD CONSTRAINT FK_F6D724BE3D6D5A0E FOREIGN KEY (spoil_chapter_id) REFERENCES chapter (id)');
        $this->addSql('CREATE INDEX IDX_F6D724BE3D6D5A0E ON post_text_block (spoil_chapter_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE text_block (id INT AUTO_INCREMENT NOT NULL, spoil_chapter_id INT DEFAULT NULL, title VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, content LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, media VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_D5AF2D7F3D6D5A0E (spoil_chapter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE text_block ADD CONSTRAINT FK_D5AF2D7F3D6D5A0E FOREIGN KEY (spoil_chapter_id) REFERENCES chapter (id)');
        $this->addSql('ALTER TABLE ability_text_block DROP FOREIGN KEY FK_17ECA89E3D6D5A0E');
        $this->addSql('DROP INDEX IDX_17ECA89E3D6D5A0E ON ability_text_block');
        $this->addSql('ALTER TABLE ability_text_block DROP spoil_chapter_id');
        $this->addSql('ALTER TABLE arc_text_block DROP FOREIGN KEY FK_986E0C443D6D5A0E');
        $this->addSql('DROP INDEX IDX_986E0C443D6D5A0E ON arc_text_block');
        $this->addSql('ALTER TABLE arc_text_block DROP spoil_chapter_id');
        $this->addSql('ALTER TABLE character_text_block DROP FOREIGN KEY FK_642394F93D6D5A0E');
        $this->addSql('DROP INDEX IDX_642394F93D6D5A0E ON character_text_block');
        $this->addSql('ALTER TABLE character_text_block DROP spoil_chapter_id');
        $this->addSql('ALTER TABLE episode_text_block DROP FOREIGN KEY FK_8A1467E13D6D5A0E');
        $this->addSql('DROP INDEX IDX_8A1467E13D6D5A0E ON episode_text_block');
        $this->addSql('ALTER TABLE episode_text_block DROP spoil_chapter_id');
        $this->addSql('ALTER TABLE organization_text_block DROP FOREIGN KEY FK_3CF4BF903D6D5A0E');
        $this->addSql('DROP INDEX IDX_3CF4BF903D6D5A0E ON organization_text_block');
        $this->addSql('ALTER TABLE organization_text_block DROP spoil_chapter_id');
        $this->addSql('ALTER TABLE post_text_block DROP FOREIGN KEY FK_F6D724BE3D6D5A0E');
        $this->addSql('DROP INDEX IDX_F6D724BE3D6D5A0E ON post_text_block');
        $this->addSql('ALTER TABLE post_text_block DROP spoil_chapter_id');
    }
}
