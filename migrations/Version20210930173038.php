<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210930173038 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE relation DROP FOREIGN KEY FK_628947496967DF41');
        $this->addSql('ALTER TABLE relation DROP FOREIGN KEY FK_6289474988A7E3A9');
        $this->addSql('DROP INDEX IDX_628947496967DF41 ON relation');
        $this->addSql('DROP INDEX IDX_6289474988A7E3A9 ON relation');
        $this->addSql('ALTER TABLE relation ADD b_id INT DEFAULT NULL, ADD a_id INT DEFAULT NULL, DROP relative_id, DROP base_id');
        $this->addSql('ALTER TABLE relation ADD CONSTRAINT FK_62894749296BFCB6 FOREIGN KEY (b_id) REFERENCES `character` (id)');
        $this->addSql('ALTER TABLE relation ADD CONSTRAINT FK_628947493BDE5358 FOREIGN KEY (a_id) REFERENCES `character` (id)');
        $this->addSql('CREATE INDEX IDX_62894749296BFCB6 ON relation (b_id)');
        $this->addSql('CREATE INDEX IDX_628947493BDE5358 ON relation (a_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE relation DROP FOREIGN KEY FK_62894749296BFCB6');
        $this->addSql('ALTER TABLE relation DROP FOREIGN KEY FK_628947493BDE5358');
        $this->addSql('DROP INDEX IDX_62894749296BFCB6 ON relation');
        $this->addSql('DROP INDEX IDX_628947493BDE5358 ON relation');
        $this->addSql('ALTER TABLE relation ADD relative_id INT DEFAULT NULL, ADD base_id INT DEFAULT NULL, DROP b_id, DROP a_id');
        $this->addSql('ALTER TABLE relation ADD CONSTRAINT FK_628947496967DF41 FOREIGN KEY (base_id) REFERENCES `character` (id)');
        $this->addSql('ALTER TABLE relation ADD CONSTRAINT FK_6289474988A7E3A9 FOREIGN KEY (relative_id) REFERENCES `character` (id)');
        $this->addSql('CREATE INDEX IDX_628947496967DF41 ON relation (base_id)');
        $this->addSql('CREATE INDEX IDX_6289474988A7E3A9 ON relation (relative_id)');
    }
}
