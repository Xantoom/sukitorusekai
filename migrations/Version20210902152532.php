<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210902152532 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE organization_chapter');
        $this->addSql('ALTER TABLE organization ADD chapter_appearance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE organization ADD CONSTRAINT FK_C1EE637CDAF009AE FOREIGN KEY (chapter_appearance_id) REFERENCES chapter (id)');
        $this->addSql('CREATE INDEX IDX_C1EE637CDAF009AE ON organization (chapter_appearance_id)');
        $this->addSql('ALTER TABLE volume DROP media');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE organization_chapter (organization_id INT NOT NULL, chapter_id INT NOT NULL, INDEX IDX_34533D6D32C8A3DE (organization_id), INDEX IDX_34533D6D579F4768 (chapter_id), PRIMARY KEY(organization_id, chapter_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE organization_chapter ADD CONSTRAINT FK_34533D6D32C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE organization_chapter ADD CONSTRAINT FK_34533D6D579F4768 FOREIGN KEY (chapter_id) REFERENCES chapter (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE organization DROP FOREIGN KEY FK_C1EE637CDAF009AE');
        $this->addSql('DROP INDEX IDX_C1EE637CDAF009AE ON organization');
        $this->addSql('ALTER TABLE organization DROP chapter_appearance_id');
        $this->addSql('ALTER TABLE volume ADD media VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
