<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210908123915 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE soundtrack_episode (soundtrack_id INT NOT NULL, episode_id INT NOT NULL, INDEX IDX_65D1135228A85C41 (soundtrack_id), INDEX IDX_65D11352362B62A0 (episode_id), PRIMARY KEY(soundtrack_id, episode_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE soundtrack_episode ADD CONSTRAINT FK_65D1135228A85C41 FOREIGN KEY (soundtrack_id) REFERENCES soundtrack (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE soundtrack_episode ADD CONSTRAINT FK_65D11352362B62A0 FOREIGN KEY (episode_id) REFERENCES episode (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE soundtrack_episode');
    }
}
