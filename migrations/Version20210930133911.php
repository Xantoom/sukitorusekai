<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210930133911 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE relation (id INT AUTO_INCREMENT NOT NULL, relative_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_6289474988A7E3A9 (relative_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE relation ADD CONSTRAINT FK_6289474988A7E3A9 FOREIGN KEY (relative_id) REFERENCES `character` (id)');
        $this->addSql('DROP TABLE character_character');
        $this->addSql('ALTER TABLE `character` DROP relations');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE character_character (character_source INT NOT NULL, character_target INT NOT NULL, INDEX IDX_142A120FFCC8BCE0 (character_source), INDEX IDX_142A120FE52DEC6F (character_target), PRIMARY KEY(character_source, character_target)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE character_character ADD CONSTRAINT FK_142A120FE52DEC6F FOREIGN KEY (character_target) REFERENCES `character` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE character_character ADD CONSTRAINT FK_142A120FFCC8BCE0 FOREIGN KEY (character_source) REFERENCES `character` (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE relation');
        $this->addSql('ALTER TABLE `character` ADD relations LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\'');
    }
}
