<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211012121033 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE arc_media (arc_id INT NOT NULL, media_id INT NOT NULL, INDEX IDX_A8ECADEF41EB8A3C (arc_id), INDEX IDX_A8ECADEFEA9FDD75 (media_id), PRIMARY KEY(arc_id, media_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE arc_media ADD CONSTRAINT FK_A8ECADEF41EB8A3C FOREIGN KEY (arc_id) REFERENCES arc (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE arc_media ADD CONSTRAINT FK_A8ECADEFEA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE arc_media');
    }
}
