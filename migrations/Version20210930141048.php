<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210930141048 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE relation ADD CONSTRAINT FK_628947496967DF41 FOREIGN KEY (base_id) REFERENCES `character` (id)');
        $this->addSql('CREATE INDEX IDX_628947496967DF41 ON relation (base_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE relation DROP FOREIGN KEY FK_628947496967DF41');
        $this->addSql('DROP INDEX IDX_628947496967DF41 ON relation');
    }
}
