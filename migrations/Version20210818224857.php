<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210818224857 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE soundtrack DROP FOREIGN KEY FK_C1B3F22363B1F25');
        $this->addSql('CREATE TABLE disc (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, romaji VARCHAR(255) NOT NULL, released_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('DROP TABLE disk');
        $this->addSql('DROP INDEX IDX_C1B3F22363B1F25 ON soundtrack');
        $this->addSql('ALTER TABLE soundtrack CHANGE disk_id disc_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE soundtrack ADD CONSTRAINT FK_C1B3F223C38F37CA FOREIGN KEY (disc_id) REFERENCES disc (id)');
        $this->addSql('CREATE INDEX IDX_C1B3F223C38F37CA ON soundtrack (disc_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE soundtrack DROP FOREIGN KEY FK_C1B3F223C38F37CA');
        $this->addSql('CREATE TABLE disk (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, romaji VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, released_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE disc');
        $this->addSql('DROP INDEX IDX_C1B3F223C38F37CA ON soundtrack');
        $this->addSql('ALTER TABLE soundtrack CHANGE disc_id disk_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE soundtrack ADD CONSTRAINT FK_C1B3F22363B1F25 FOREIGN KEY (disk_id) REFERENCES disk (id)');
        $this->addSql('CREATE INDEX IDX_C1B3F22363B1F25 ON soundtrack (disk_id)');
    }
}
