<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211012222525 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bluray DROP FOREIGN KEY FK_F697EA50922726E9');
        $this->addSql('DROP INDEX IDX_F697EA50922726E9 ON bluray');
        $this->addSql('ALTER TABLE bluray DROP cover_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bluray ADD cover_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bluray ADD CONSTRAINT FK_F697EA50922726E9 FOREIGN KEY (cover_id) REFERENCES media (id)');
        $this->addSql('CREATE INDEX IDX_F697EA50922726E9 ON bluray (cover_id)');
    }
}
