<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211001222724 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE character_media');
        $this->addSql('ALTER TABLE `character` DROP FOREIGN KEY FK_937AB034922726E9');
        $this->addSql('DROP INDEX IDX_937AB034922726E9 ON `character`');
        $this->addSql('ALTER TABLE `character` DROP cover_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE character_media (character_id INT NOT NULL, media_id INT NOT NULL, INDEX IDX_94EF466E1136BE75 (character_id), INDEX IDX_94EF466EEA9FDD75 (media_id), PRIMARY KEY(character_id, media_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE character_media ADD CONSTRAINT FK_94EF466E1136BE75 FOREIGN KEY (character_id) REFERENCES `character` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE character_media ADD CONSTRAINT FK_94EF466EEA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE `character` ADD cover_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE `character` ADD CONSTRAINT FK_937AB034922726E9 FOREIGN KEY (cover_id) REFERENCES media (id)');
        $this->addSql('CREATE INDEX IDX_937AB034922726E9 ON `character` (cover_id)');
    }
}
