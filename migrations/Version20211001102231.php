<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211001102231 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE relation (id INT AUTO_INCREMENT NOT NULL, base_id INT DEFAULT NULL, relative_id INT DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, INDEX IDX_628947496967DF41 (base_id), INDEX IDX_6289474988A7E3A9 (relative_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE relation ADD CONSTRAINT FK_628947496967DF41 FOREIGN KEY (base_id) REFERENCES `character` (id)');
        $this->addSql('ALTER TABLE relation ADD CONSTRAINT FK_6289474988A7E3A9 FOREIGN KEY (relative_id) REFERENCES `character` (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE relation');
    }
}
