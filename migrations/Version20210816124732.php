<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210816124732 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE chapter_key_moment');
        $this->addSql('DROP TABLE key_moment_character');
        $this->addSql('DROP TABLE key_moment_soundtrack');
        $this->addSql('ALTER TABLE key_moment DROP FOREIGN KEY FK_3EB569FE362B62A0');
        $this->addSql('DROP INDEX IDX_3EB569FE362B62A0 ON key_moment');
        $this->addSql('ALTER TABLE key_moment DROP episode_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE chapter_key_moment (chapter_id INT NOT NULL, key_moment_id INT NOT NULL, INDEX IDX_70C16647579F4768 (chapter_id), INDEX IDX_70C16647D62F0C39 (key_moment_id), PRIMARY KEY(chapter_id, key_moment_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE key_moment_character (key_moment_id INT NOT NULL, character_id INT NOT NULL, INDEX IDX_46E99C30D62F0C39 (key_moment_id), INDEX IDX_46E99C301136BE75 (character_id), PRIMARY KEY(key_moment_id, character_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE key_moment_soundtrack (key_moment_id INT NOT NULL, soundtrack_id INT NOT NULL, INDEX IDX_C60BA516D62F0C39 (key_moment_id), INDEX IDX_C60BA51628A85C41 (soundtrack_id), PRIMARY KEY(key_moment_id, soundtrack_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE chapter_key_moment ADD CONSTRAINT FK_70C16647579F4768 FOREIGN KEY (chapter_id) REFERENCES chapter (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE chapter_key_moment ADD CONSTRAINT FK_70C16647D62F0C39 FOREIGN KEY (key_moment_id) REFERENCES key_moment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE key_moment_character ADD CONSTRAINT FK_46E99C301136BE75 FOREIGN KEY (character_id) REFERENCES `character` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE key_moment_character ADD CONSTRAINT FK_46E99C30D62F0C39 FOREIGN KEY (key_moment_id) REFERENCES key_moment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE key_moment_soundtrack ADD CONSTRAINT FK_C60BA51628A85C41 FOREIGN KEY (soundtrack_id) REFERENCES soundtrack (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE key_moment_soundtrack ADD CONSTRAINT FK_C60BA516D62F0C39 FOREIGN KEY (key_moment_id) REFERENCES key_moment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE key_moment ADD episode_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE key_moment ADD CONSTRAINT FK_3EB569FE362B62A0 FOREIGN KEY (episode_id) REFERENCES episode (id)');
        $this->addSql('CREATE INDEX IDX_3EB569FE362B62A0 ON key_moment (episode_id)');
    }
}
