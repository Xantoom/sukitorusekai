<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210809152539 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE cd (id INT AUTO_INCREMENT NOT NULL, titles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', release_dates LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', slug VARCHAR(255) NOT NULL, notes LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cd_soundtrack (cd_id INT NOT NULL, soundtrack_id INT NOT NULL, INDEX IDX_5E6614D235F486F6 (cd_id), INDEX IDX_5E6614D228A85C41 (soundtrack_id), PRIMARY KEY(cd_id, soundtrack_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE soundtrack (id INT AUTO_INCREMENT NOT NULL, titles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', number INT NOT NULL, duration INT NOT NULL, staff LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', notes LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cd_soundtrack ADD CONSTRAINT FK_5E6614D235F486F6 FOREIGN KEY (cd_id) REFERENCES cd (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE cd_soundtrack ADD CONSTRAINT FK_5E6614D228A85C41 FOREIGN KEY (soundtrack_id) REFERENCES soundtrack (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cd_soundtrack DROP FOREIGN KEY FK_5E6614D235F486F6');
        $this->addSql('ALTER TABLE cd_soundtrack DROP FOREIGN KEY FK_5E6614D228A85C41');
        $this->addSql('DROP TABLE cd');
        $this->addSql('DROP TABLE cd_soundtrack');
        $this->addSql('DROP TABLE soundtrack');
    }
}
