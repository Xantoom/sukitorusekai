<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210810133047 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ability (id INT AUTO_INCREMENT NOT NULL, names LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', slug VARCHAR(255) NOT NULL, descriptions LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', techniques LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', notes LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE arc (id INT AUTO_INCREMENT NOT NULL, titles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', number INT NOT NULL, medias LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', synopsis LONGTEXT NOT NULL, slug VARCHAR(255) NOT NULL, notes LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bluray (id INT AUTO_INCREMENT NOT NULL, titles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', slug VARCHAR(255) NOT NULL, release_date DATE NOT NULL, notes LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE chapter (id INT AUTO_INCREMENT NOT NULL, volume_id INT DEFAULT NULL, arc_id INT DEFAULT NULL, episode_id INT DEFAULT NULL, titles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', number INT NOT NULL, pages_amount INT NOT NULL, staff LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', synopsis LONGTEXT NOT NULL, script LONGTEXT NOT NULL, medias LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', notes LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', slug VARCHAR(255) NOT NULL, INDEX IDX_F981B52E8FD80EEA (volume_id), INDEX IDX_F981B52E41EB8A3C (arc_id), INDEX IDX_F981B52E362B62A0 (episode_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE chapter_character (chapter_id INT NOT NULL, character_id INT NOT NULL, INDEX IDX_E3F65DD6579F4768 (chapter_id), INDEX IDX_E3F65DD61136BE75 (character_id), PRIMARY KEY(chapter_id, character_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE chapter_key_moment (chapter_id INT NOT NULL, key_moment_id INT NOT NULL, INDEX IDX_70C16647579F4768 (chapter_id), INDEX IDX_70C16647D62F0C39 (key_moment_id), PRIMARY KEY(chapter_id, key_moment_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `character` (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(255) NOT NULL, names LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', aliases LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', age LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', height LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', weight LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', race LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', status LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', gender VARCHAR(255) NOT NULL, relatives LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', appareance LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', voice_actors LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', medias LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE character_organization (character_id INT NOT NULL, organization_id INT NOT NULL, INDEX IDX_1E8AE6151136BE75 (character_id), INDEX IDX_1E8AE61532C8A3DE (organization_id), PRIMARY KEY(character_id, organization_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE character_ability (character_id INT NOT NULL, ability_id INT NOT NULL, INDEX IDX_5C6A6E011136BE75 (character_id), INDEX IDX_5C6A6E018016D8B2 (ability_id), PRIMARY KEY(character_id, ability_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE character_episode (character_id INT NOT NULL, episode_id INT NOT NULL, INDEX IDX_B40F9CE71136BE75 (character_id), INDEX IDX_B40F9CE7362B62A0 (episode_id), PRIMARY KEY(character_id, episode_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, author_id INT NOT NULL, post_id INT NOT NULL, content LONGTEXT NOT NULL, INDEX IDX_9474526CF675F31B (author_id), INDEX IDX_9474526C4B89032C (post_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE episode (id INT AUTO_INCREMENT NOT NULL, season_id INT NOT NULL, bluray_id INT DEFAULT NULL, titles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', number INT NOT NULL, duration INT NOT NULL, release_dates LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', staff LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', synopsis LONGTEXT NOT NULL, script LONGTEXT NOT NULL, medias LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', notes LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', links LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', slug VARCHAR(255) NOT NULL, INDEX IDX_DDAA1CDA4EC001D1 (season_id), INDEX IDX_DDAA1CDAC007F6F7 (bluray_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE key_moment (id INT AUTO_INCREMENT NOT NULL, episode_id INT DEFAULT NULL, titles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', timecodes LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', pages LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', script LONGTEXT NOT NULL, medias LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', slug VARCHAR(255) NOT NULL, notes LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_3EB569FE362B62A0 (episode_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE key_moment_character (key_moment_id INT NOT NULL, character_id INT NOT NULL, INDEX IDX_46E99C30D62F0C39 (key_moment_id), INDEX IDX_46E99C301136BE75 (character_id), PRIMARY KEY(key_moment_id, character_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE key_moment_soundtrack (key_moment_id INT NOT NULL, soundtrack_id INT NOT NULL, INDEX IDX_C60BA516D62F0C39 (key_moment_id), INDEX IDX_C60BA51628A85C41 (soundtrack_id), PRIMARY KEY(key_moment_id, soundtrack_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE organization (id INT AUTO_INCREMENT NOT NULL, names LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', notes LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', slug VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE post (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(255) NOT NULL, titles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', content LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE post_user (post_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_44C6B1424B89032C (post_id), INDEX IDX_44C6B142A76ED395 (user_id), PRIMARY KEY(post_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE post_chapter (post_id INT NOT NULL, chapter_id INT NOT NULL, INDEX IDX_C469021D4B89032C (post_id), INDEX IDX_C469021D579F4768 (chapter_id), PRIMARY KEY(post_id, chapter_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE season (id INT AUTO_INCREMENT NOT NULL, number INT NOT NULL, titles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', medias LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', release_dates LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', synopsis LONGTEXT NOT NULL, slug VARCHAR(255) NOT NULL, notes LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, current_chapter_id INT NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, pseudo VARCHAR(255) NOT NULL, avatar VARCHAR(255) NOT NULL, options LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_8D93D64988248E1A (current_chapter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE volume (id INT AUTO_INCREMENT NOT NULL, number INT NOT NULL, titles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', medias LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', release_dates LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', synopsis LONGTEXT NOT NULL, slug VARCHAR(255) NOT NULL, notes LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE chapter ADD CONSTRAINT FK_F981B52E8FD80EEA FOREIGN KEY (volume_id) REFERENCES volume (id)');
        $this->addSql('ALTER TABLE chapter ADD CONSTRAINT FK_F981B52E41EB8A3C FOREIGN KEY (arc_id) REFERENCES arc (id)');
        $this->addSql('ALTER TABLE chapter ADD CONSTRAINT FK_F981B52E362B62A0 FOREIGN KEY (episode_id) REFERENCES episode (id)');
        $this->addSql('ALTER TABLE chapter_character ADD CONSTRAINT FK_E3F65DD6579F4768 FOREIGN KEY (chapter_id) REFERENCES chapter (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE chapter_character ADD CONSTRAINT FK_E3F65DD61136BE75 FOREIGN KEY (character_id) REFERENCES `character` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE chapter_key_moment ADD CONSTRAINT FK_70C16647579F4768 FOREIGN KEY (chapter_id) REFERENCES chapter (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE chapter_key_moment ADD CONSTRAINT FK_70C16647D62F0C39 FOREIGN KEY (key_moment_id) REFERENCES key_moment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE character_organization ADD CONSTRAINT FK_1E8AE6151136BE75 FOREIGN KEY (character_id) REFERENCES `character` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE character_organization ADD CONSTRAINT FK_1E8AE61532C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE character_ability ADD CONSTRAINT FK_5C6A6E011136BE75 FOREIGN KEY (character_id) REFERENCES `character` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE character_ability ADD CONSTRAINT FK_5C6A6E018016D8B2 FOREIGN KEY (ability_id) REFERENCES ability (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE character_episode ADD CONSTRAINT FK_B40F9CE71136BE75 FOREIGN KEY (character_id) REFERENCES `character` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE character_episode ADD CONSTRAINT FK_B40F9CE7362B62A0 FOREIGN KEY (episode_id) REFERENCES episode (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C4B89032C FOREIGN KEY (post_id) REFERENCES post (id)');
        $this->addSql('ALTER TABLE episode ADD CONSTRAINT FK_DDAA1CDA4EC001D1 FOREIGN KEY (season_id) REFERENCES season (id)');
        $this->addSql('ALTER TABLE episode ADD CONSTRAINT FK_DDAA1CDAC007F6F7 FOREIGN KEY (bluray_id) REFERENCES bluray (id)');
        $this->addSql('ALTER TABLE key_moment ADD CONSTRAINT FK_3EB569FE362B62A0 FOREIGN KEY (episode_id) REFERENCES episode (id)');
        $this->addSql('ALTER TABLE key_moment_character ADD CONSTRAINT FK_46E99C30D62F0C39 FOREIGN KEY (key_moment_id) REFERENCES key_moment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE key_moment_character ADD CONSTRAINT FK_46E99C301136BE75 FOREIGN KEY (character_id) REFERENCES `character` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE key_moment_soundtrack ADD CONSTRAINT FK_C60BA516D62F0C39 FOREIGN KEY (key_moment_id) REFERENCES key_moment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE key_moment_soundtrack ADD CONSTRAINT FK_C60BA51628A85C41 FOREIGN KEY (soundtrack_id) REFERENCES soundtrack (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE post_user ADD CONSTRAINT FK_44C6B1424B89032C FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE post_user ADD CONSTRAINT FK_44C6B142A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE post_chapter ADD CONSTRAINT FK_C469021D4B89032C FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE post_chapter ADD CONSTRAINT FK_C469021D579F4768 FOREIGN KEY (chapter_id) REFERENCES chapter (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64988248E1A FOREIGN KEY (current_chapter_id) REFERENCES chapter (id)');
        $this->addSql('ALTER TABLE soundtrack ADD episode_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE soundtrack ADD CONSTRAINT FK_C1B3F223362B62A0 FOREIGN KEY (episode_id) REFERENCES episode (id)');
        $this->addSql('CREATE INDEX IDX_C1B3F223362B62A0 ON soundtrack (episode_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE character_ability DROP FOREIGN KEY FK_5C6A6E018016D8B2');
        $this->addSql('ALTER TABLE chapter DROP FOREIGN KEY FK_F981B52E41EB8A3C');
        $this->addSql('ALTER TABLE episode DROP FOREIGN KEY FK_DDAA1CDAC007F6F7');
        $this->addSql('ALTER TABLE chapter_character DROP FOREIGN KEY FK_E3F65DD6579F4768');
        $this->addSql('ALTER TABLE chapter_key_moment DROP FOREIGN KEY FK_70C16647579F4768');
        $this->addSql('ALTER TABLE post_chapter DROP FOREIGN KEY FK_C469021D579F4768');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64988248E1A');
        $this->addSql('ALTER TABLE chapter_character DROP FOREIGN KEY FK_E3F65DD61136BE75');
        $this->addSql('ALTER TABLE character_organization DROP FOREIGN KEY FK_1E8AE6151136BE75');
        $this->addSql('ALTER TABLE character_ability DROP FOREIGN KEY FK_5C6A6E011136BE75');
        $this->addSql('ALTER TABLE character_episode DROP FOREIGN KEY FK_B40F9CE71136BE75');
        $this->addSql('ALTER TABLE key_moment_character DROP FOREIGN KEY FK_46E99C301136BE75');
        $this->addSql('ALTER TABLE chapter DROP FOREIGN KEY FK_F981B52E362B62A0');
        $this->addSql('ALTER TABLE character_episode DROP FOREIGN KEY FK_B40F9CE7362B62A0');
        $this->addSql('ALTER TABLE key_moment DROP FOREIGN KEY FK_3EB569FE362B62A0');
        $this->addSql('ALTER TABLE soundtrack DROP FOREIGN KEY FK_C1B3F223362B62A0');
        $this->addSql('ALTER TABLE chapter_key_moment DROP FOREIGN KEY FK_70C16647D62F0C39');
        $this->addSql('ALTER TABLE key_moment_character DROP FOREIGN KEY FK_46E99C30D62F0C39');
        $this->addSql('ALTER TABLE key_moment_soundtrack DROP FOREIGN KEY FK_C60BA516D62F0C39');
        $this->addSql('ALTER TABLE character_organization DROP FOREIGN KEY FK_1E8AE61532C8A3DE');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C4B89032C');
        $this->addSql('ALTER TABLE post_user DROP FOREIGN KEY FK_44C6B1424B89032C');
        $this->addSql('ALTER TABLE post_chapter DROP FOREIGN KEY FK_C469021D4B89032C');
        $this->addSql('ALTER TABLE episode DROP FOREIGN KEY FK_DDAA1CDA4EC001D1');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CF675F31B');
        $this->addSql('ALTER TABLE post_user DROP FOREIGN KEY FK_44C6B142A76ED395');
        $this->addSql('ALTER TABLE chapter DROP FOREIGN KEY FK_F981B52E8FD80EEA');
        $this->addSql('DROP TABLE ability');
        $this->addSql('DROP TABLE arc');
        $this->addSql('DROP TABLE bluray');
        $this->addSql('DROP TABLE chapter');
        $this->addSql('DROP TABLE chapter_character');
        $this->addSql('DROP TABLE chapter_key_moment');
        $this->addSql('DROP TABLE `character`');
        $this->addSql('DROP TABLE character_organization');
        $this->addSql('DROP TABLE character_ability');
        $this->addSql('DROP TABLE character_episode');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE episode');
        $this->addSql('DROP TABLE key_moment');
        $this->addSql('DROP TABLE key_moment_character');
        $this->addSql('DROP TABLE key_moment_soundtrack');
        $this->addSql('DROP TABLE organization');
        $this->addSql('DROP TABLE post');
        $this->addSql('DROP TABLE post_user');
        $this->addSql('DROP TABLE post_chapter');
        $this->addSql('DROP TABLE season');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE volume');
        $this->addSql('DROP INDEX IDX_C1B3F223362B62A0 ON soundtrack');
        $this->addSql('ALTER TABLE soundtrack DROP episode_id');
    }
}
