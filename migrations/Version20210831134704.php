<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210831134704 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `character` ADD first_chapter_appearance_id INT NOT NULL');
        $this->addSql('ALTER TABLE `character` ADD CONSTRAINT FK_937AB0345F89272A FOREIGN KEY (first_chapter_appearance_id) REFERENCES chapter (id)');
        $this->addSql('CREATE INDEX IDX_937AB0345F89272A ON `character` (first_chapter_appearance_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `character` DROP FOREIGN KEY FK_937AB0345F89272A');
        $this->addSql('DROP INDEX IDX_937AB0345F89272A ON `character`');
        $this->addSql('ALTER TABLE `character` DROP first_chapter_appearance_id');
    }
}
