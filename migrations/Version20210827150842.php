<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210827150842 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE media ADD updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE post ADD cover_id INT DEFAULT NULL, DROP cover');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8D922726E9 FOREIGN KEY (cover_id) REFERENCES media (id)');
        $this->addSql('CREATE INDEX IDX_5A8A6C8D922726E9 ON post (cover_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE media DROP updated_at');
        $this->addSql('ALTER TABLE post DROP FOREIGN KEY FK_5A8A6C8D922726E9');
        $this->addSql('DROP INDEX IDX_5A8A6C8D922726E9 ON post');
        $this->addSql('ALTER TABLE post ADD cover VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP cover_id');
    }
}
