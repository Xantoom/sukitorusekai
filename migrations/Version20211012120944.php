<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211012120944 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE ability_media');
        $this->addSql('ALTER TABLE arc DROP FOREIGN KEY FK_7FE65393922726E9');
        $this->addSql('DROP INDEX IDX_7FE65393922726E9 ON arc');
        $this->addSql('ALTER TABLE arc DROP cover_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ability_media (ability_id INT NOT NULL, media_id INT NOT NULL, INDEX IDX_8C62AD38016D8B2 (ability_id), INDEX IDX_8C62AD3EA9FDD75 (media_id), PRIMARY KEY(ability_id, media_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE ability_media ADD CONSTRAINT FK_8C62AD38016D8B2 FOREIGN KEY (ability_id) REFERENCES ability (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ability_media ADD CONSTRAINT FK_8C62AD3EA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE arc ADD cover_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE arc ADD CONSTRAINT FK_7FE65393922726E9 FOREIGN KEY (cover_id) REFERENCES media (id)');
        $this->addSql('CREATE INDEX IDX_7FE65393922726E9 ON arc (cover_id)');
    }
}
