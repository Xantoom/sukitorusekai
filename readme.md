# SukitoruSekai

SukitoruSekai is a website about the manga : Kimetsu no Yaiba.

## Development environment

### Requirements
- PHP 7.4
- Composer
- Symfony CLI
- NodeJS & NPM

You can check requirements with the following command :
- symfony check:requirements

### Launch env-dev
- composer install
- npm install
- npm run build
- symfony serve -d

### Launch DataFixtures for test
- symfony console doctrine:fixtures:load

### Launch Tests
php bin/phpunit --testdox

