function manageComment(path) {
    $.ajax({
        url: path,
        async: true,
        cache: false,
    });
}

const buttons = document.querySelectorAll("#comment");

buttons.forEach(button => {
    button.addEventListener("click", () => {
        manageComment(button.getAttribute("data-id"));
        if(button.textContent === "Hide") {
            button.classList.remove("btn-danger-soft");
            button.classList.add("btn-success-soft");
            button.textContent = "Show";
            button.parentElement.parentElement.parentElement.lastElementChild.classList.remove("text-info");
            button.parentElement.parentElement.parentElement.lastElementChild.classList.add("text-muted");
        } else {
            button.classList.remove("btn-success-soft");
            button.classList.add("btn-danger-soft");
            button.textContent = "Hide";
            button.parentElement.parentElement.parentElement.lastElementChild.classList.remove("text-muted");
            button.parentElement.parentElement.parentElement.lastElementChild.classList.add("text-info");
        }
    });
});
