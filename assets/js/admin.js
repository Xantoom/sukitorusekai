const characterRelatives = document.querySelector("#character_relations");
let characterIndex = characterRelatives.querySelectorAll("fieldset").length;

const addRelative = () => {
    characterRelatives.innerHTML += characterRelatives.dataset.prototype.replace(/__name__/g, characterIndex);
    characterIndex++;
}

document.querySelector("#addRelative").addEventListener("click", addRelative);

/*const collection = document.querySelector("#ability_medias");
let index = collection.querySelectorAll("fieldset").length;

const addMedia = () => {
    collection.innerHTML += collection.dataset.prototype.replace(/__name__/g, index);
    index++;
}

const removeMedia = () => {

}

document.querySelector("#addMedia").addEventListener("click", addMedia);
document.querySelector("#removeMedia").addEventListener("click", removeMedia);*/