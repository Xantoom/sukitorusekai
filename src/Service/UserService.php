<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Security;

class UserService {

    private UserRepository $userRepository;
    private Security $security;

    public function __construct(UserRepository $userRepository, Security $security) {
        $this->userRepository = $userRepository;
        $this->security = $security;
    }

    /**
     * Return the current connected User, return null either.
     * @return User|null
     */
    public function getCurrentUser(): ?User {
        if(!$this->security->getUser()) return null;
        return $this->userRepository->findOneBy(["email" => $this->security->getUser()->getUserIdentifier()]);
    }

}