<?php

namespace App\Service;

use App\Entity\Ability;
use App\Entity\Arc;
use App\Entity\Bluray;
use App\Entity\Category;
use App\Entity\Chapter;
use App\Entity\Character;
use App\Entity\Comment;
use App\Entity\Disc;
use App\Entity\Edit;
use App\Entity\Episode;
use App\Entity\Organization;
use App\Entity\Post;
use App\Entity\Relation;
use App\Entity\Season;
use App\Entity\Soundtrack;
use App\Entity\User;
use App\Entity\Volume;
use App\Repository\RelationRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;

class PersistService {

    private SluggerInterface $slugger;
    private EntityManagerInterface $manager;
    private RelationService $relationService;
    private RequestStack $requestStack;
    private Request $request;
    private Security $security;
    private RelationRepository $relationRepository;
    private UserRepository $userRepository;

    public function __construct(
        SluggerInterface $slugger, EntityManagerInterface $manager,
        RelationService $relationService, RequestStack $requestStack,
        Security $security, UserRepository $userRepository,
        RelationRepository $relationRepository
    ) {
        $this->slugger = $slugger;
        $this->manager = $manager;
        $this->relationService = $relationService;
        $this->requestStack = $requestStack;
        $this->security = $security;
        $this->relationRepository = $relationRepository;
        $this->userRepository = $userRepository;
    }

    public function configure($entity) {
        $this->request = $this->requestStack->getCurrentRequest();
        if($entity instanceof Ability)      return $this->onBeforePersistAbility($entity);
        if($entity instanceof Arc)          return $this->onBeforePersistArc($entity);
        if($entity instanceof Bluray)       return $this->onBeforePersistBluray($entity);
        if($entity instanceof Category)     return $this->onBeforePersistCategory($entity);
        if($entity instanceof Chapter)      return $this->onBeforePersistChapter($entity);
        if($entity instanceof Character)    return $this->onBeforePersistCharacter($entity);
        if($entity instanceof Comment)      return $this->onBeforePersistComment($entity);
        if($entity instanceof Disc)         return $this->onBeforePersistDisc($entity);
        if($entity instanceof Episode)      return $this->onBeforePersistEpisode($entity);
        if($entity instanceof Organization) return $this->onBeforePersistOrganization($entity);
        if($entity instanceof Post)         return $this->onBeforePersistPost($entity);
        if($entity instanceof Season)       return $this->onBeforePersistSeason($entity);
        if($entity instanceof Soundtrack)   return $this->onBeforePersistSoundtrack($entity);
        if($entity instanceof User)         return $this->onBeforePersistUser($entity);
        if($entity instanceof Volume)       return $this->onBeforePersistVolume($entity);
        return null;
    }

    /**
     * Configure the Ability before the manager persist it.
     *
     * @param Ability $ability
     * @return Ability
     */
    private function onBeforePersistAbility(Ability $ability): Ability {
        $ability->setSlug(strtolower($this->slugger->slug($ability->getName())));
        return $ability;
    }

    /**
     * Configure the Arc before the manager persist it.
     *
     * @param Arc $arc
     * @return Arc
     */
    private function onBeforePersistArc(Arc $arc): Arc {
        $arc->setSlug(strtolower($this->slugger->slug($arc->getTitle())));
        return $arc;
    }

    /**
     * Configure the Bluray before the manager persist it.
     *
     * @param Bluray $bluray
     * @return Bluray
     */
    private function onBeforePersistBluray(Bluray $bluray): Bluray {
        $bluray->setSlug(strtolower($this->slugger->slug($bluray->getTitle())));
        return $bluray;
    }

    /**
     * Configure the Category before the manager persist it.
     *
     * @param Category $category
     * @return Category
     */
    private function onBeforePersistCategory(Category $category): Category {
        $category->setSlug(strtolower($this->slugger->slug($category->getTitle())));
        return $category;
    }

    /**
     * Configure the Chapter before the manager persist it.
     *
     * @param Chapter $chapter
     * @return Chapter
     */
    private function onBeforePersistChapter(Chapter $chapter): Chapter {
        return $chapter;
    }

    /**
     * Configure the Character before the manager persist it.
     *
     * @param Character $character
     * @return Character
     */
    private function onBeforePersistCharacter(Character $character): Character {
        $character->setSlug(strtolower($this->slugger->slug($character->getName())));
        $relations = $character->getRelations();
        foreach($relations as $relation) {
            $rel = new Relation();
            $rel->setBase($relation->getRelative())
                ->setType($this->relationService->getInversedRelationType($character->getGender(), $relation->getType()))
                ->setRelative($character);
            if($this->relationRepository->findOneBy(["base" => $relation->getBase()->getId(), "relative" => $relation->getRelative()->getId()])) continue;
            if($this->relationRepository->findOneBy(["base" => $rel->getBase()->getId(), "relative" => $rel->getRelative()->getId()])) continue;
            $this->manager->persist($rel);
        }
        return $character;
    }

    /**
     * Configure the Comment before the manager persist it.
     *
     * @param Comment $comment
     * @return Comment
     */
    private function onBeforePersistComment(Comment $comment): Comment {
        if(!$comment->getIsMuted()) {
            $user = $this->userRepository->findOneBy(["email" => $this->security->getUser()->getUserIdentifier()]);
            $comment
                ->setAuthor($user)
                ->setSentAt(new \DateTime("now"))
                ->setIsMuted(false)
            ;
        }
        return $comment;
    }

    /**
     * Configure the Disc before the manager persist it.
     *
     * @param Disc $disc
     * @return Disc
     */
    private function onBeforePersistDisc(Disc $disc): Disc {
        $disc->setSlug(strtolower($this->slugger->slug($disc->getTitle())));
        return $disc;
    }

    /**
     * Configure the Episode before the manager persist it.
     *
     * @param Episode $episode
     * @return Episode
     */
    private function onBeforePersistEpisode(Episode $episode): Episode {
        $episode->setSlug(strtolower($this->slugger->slug($episode->getTitle())));
        return $episode;
    }

    /**
     * Configure the Organization before the manager persist it.
     *
     * @param Organization $organization
     * @return Organization
     */
    private function onBeforePersistOrganization(Organization $organization): Organization {
        $organization->setSlug(strtolower($this->slugger->slug($organization->getName())));
        return $organization;
    }

    /**
     * Configure the Post before the manager persist it.
     *
     * @param Post $post
     * @return Post
     */
    private function onBeforePersistPost(Post $post): Post {
        $post->setSlug(strtolower($this->slugger->slug($post->getTitle())));
        $user = $this->userRepository->findOneBy(["email" => $this->security->getUser()->getUserIdentifier()]);
        if($post->getAuthor() == null)
            $post
                ->setAuthor($user)
                ->setIsPublished(false)
                ->setPostedAt(new \DateTime("now"));
        else {
            $edit = new Edit();
            $edit
                ->setEditor($user)
                ->setEditedAt(new \DateTime("now"))
                ->setPost($post);
            $post->addEdit($edit);
            $this->manager->persist($edit);
        }
        return $post;
    }

    /**
     * Configure the Season before the manager persist it.
     *
     * @param Season $season
     * @return Season
     */
    private function onBeforePersistSeason(Season $season): Season {
        $season->setSlug(strtolower($this->slugger->slug($season->getTitle())));
        return $season;
    }

    /**
     * Configure the Soundtrack before the manager persist it.
     *
     * @param Soundtrack $soundtrack
     * @return Soundtrack
     */
    private function onBeforePersistSoundtrack(Soundtrack $soundtrack): Soundtrack {
        return $soundtrack;
    }

    /**
     * Configure the User before the manager persist it.
     *
     * @param User $user
     * @return User
     */
    private function onBeforePersistUser(User $user): User {
        return $user;
    }

    /**
     * Configure the Volume before the manager persist it.
     *
     * @param Volume $volume
     * @return Volume
     */
    private function onBeforePersistVolume(Volume $volume): Volume {
        $volume->setSlug(strtolower($this->slugger->slug($volume->getTitle())));
        return $volume;
    }
}