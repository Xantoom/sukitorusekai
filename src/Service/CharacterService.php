<?php

namespace App\Service;

use App\Repository\CharacterRepository;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Security;

class CharacterService {

    private CharacterRepository $characterRepository;
    private UserService $userService;

    public function __construct(CharacterRepository $characterRepository, UserService $userService) {
        $this->characterRepository = $characterRepository;
        $this->userService = $userService;
    }

    /**
     * Return "maxCharacters" (All if null) Characters sort by Views.
     * @param int|null $maxCharacters
     * @return array
     */
    public function getCharactersByViews(int $maxCharacters = 0): array {
        $characters = $this->characterRepository->getAllCharactersByViews();
        $user = $this->userService->getCurrentUser();
        if(($maxCharacters == 0) || (!$user)) return $characters;
        else {
            $chs = [];
            foreach($characters as $character) {
                if($character->getFirstChapterAppearance()->getNumber() <= $user->getCurrentChapter()->getNumber())
                    $chs[] = $character;
            }
            return array_splice($chs, 0, $maxCharacters);
        }
    }

}