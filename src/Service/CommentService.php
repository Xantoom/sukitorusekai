<?php

namespace App\Service;

use App\Entity\Comment;
use App\Entity\Post;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class CommentService {

    private FlashBagInterface $flashBag;

    public function __construct(FlashBagInterface $flashBag) {
        $this->flashBag = $flashBag;
    }

    /**
     * Check if the content of a comment is valid.
     *
     * @param Comment $comment
     * @param Post $post
     * @return bool
     */
    public function checkComment(Comment  $comment, Post $post): bool {
        $content = $comment->getContent();
        if((strlen($content) > 128) && (!str_contains($content, " "))) {
            $this->flashBag->add("error", "Your comment have more than 128 characters and does not contain spaces");
            return false;
        }
        foreach($post->getComments() as $postComment) {
            if($content == $postComment->getContent()) {
                $this->flashBag->add("error", "Your already said that on this Post.");
                return false;
            }
        }
        return true;
    }

}