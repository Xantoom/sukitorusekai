<?php

namespace App\Service;

class RelationService {

    /**
     * Get the inverse relation as a string. (ex: Sister -> Brother)
     *
     * @param string $gender
     * @param string $relationType
     * @return string
     */
    public function getInversedRelationType(string $gender, string $relationType): string {
        if(strtolower($gender) === "female") {
            switch(strtolower($relationType)) {
                case "sister":
                case "brother": return "Sister";
                case "father":
                case "mother": return "Daughter";
                case "daughter":
                case "son": return "Mother";
                case "grandmother":
                case "grandfather": return "GrandDaughter";
                case "grandson":
                case "granddaughter": return "GrandMother";
                case "aunt":
                case "uncle": return "Nephew";
                case "nephew":
                case "niece": return "Uncle";
                case "sister-in-law":
                case "brother-in-law": return "Sister-in-Law";
                case "father-in-law":
                case "mother-in-law": return "Daughter-in-Law";
                case "son-in-law":
                case "daughter-in-law": return "Mother-in-Law";
                case "husband": return "Wife";
                case "cousin": return "Cousin";
                case "ancestor": return "Descendant";
                default: return "Relative";
            }
        } else {
            switch(strtolower($relationType)) {
                case "sister":
                case "brother": return "Brother";
                case "father":
                case "mother": return "Son";
                case "daughter":
                case "son": return "Father";
                case "grandmother":
                case "grandfather": return "GrandSon";
                case "grandson":
                case "granddaughter": return "GrandFather";
                case "aunt":
                case "uncle": return "Nephew";
                case "nephew":
                case "niece": return "Uncle";
                case "sister-in-law":
                case "brother-in-law": return "Brother-in-Law";
                case "father-in-law":
                case "mother-in-law": return "Son-in-Law";
                case "son-in-law":
                case "daughter-in-law": return "Father-in-Law";
                case "wife": return "Husband";
                case "cousin": return "Cousin";
                case "ancestor": return "Descendant";
                default: return "Relative";
            }
        }
    }

}