<?php

namespace App\EventListener;

use App\Entity\Ability;
use App\Entity\Arc;
use App\Entity\Bluray;
use App\Entity\Category;
use App\Entity\Chapter;
use App\Entity\Character;
use App\Entity\Comment;
use App\Entity\Disc;
use App\Entity\Edit;
use App\Entity\Episode;
use App\Entity\Organization;
use App\Entity\Post;
use App\Entity\Relation;
use App\Entity\Season;
use App\Entity\Soundtrack;
use App\Entity\User;
use App\Entity\Volume;
use App\Repository\UserRepository;
use App\Service\RelationService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;

class EasyAdminSubscriber implements EventSubscriberInterface {

    private SluggerInterface $slugger;
    private EntityManagerInterface $manager;
    private RelationService $relationService;
    private RequestStack $requestStack;
    private Request $request;
    private Security $security;
    private UserRepository $userRepository;

    public function __construct(SluggerInterface $slugger, EntityManagerInterface $manager, RelationService $relationService, RequestStack $requestStack, Security $security, UserRepository $userRepository) {
        $this->slugger = $slugger;
        $this->manager = $manager;
        $this->relationService = $relationService;
        $this->requestStack = $requestStack;
        $this->security = $security;
        $this->userRepository = $userRepository;
    }

    public static function getSubscribedEvents() {
        return [
            //BeforeEntityPersistedEvent::class => ['prePersist'],
        ];
    }

    public function prePersist(BeforeEntityPersistedEvent $event) {
        dd($event);
        $this->request = $this->requestStack->getCurrentRequest();
        $entity = $event->getEntityInstance();
        if($entity instanceof Ability)      $entity = $this->onBeforePersistAbility($entity);
        if($entity instanceof Arc)          $entity = $this->onBeforePersistArc($entity);
        if($entity instanceof Bluray)       $entity = $this->onBeforePersistBluray($entity);
        if($entity instanceof Category)     $entity = $this->onBeforePersistCategory($entity);
        if($entity instanceof Chapter)      $entity = $this->onBeforePersistChapter($entity);
        if($entity instanceof Character)    $entity = $this->onBeforePersistCharacter($entity);
        if($entity instanceof Comment)      $entity = $this->onBeforePersistComment($entity);
        if($entity instanceof Disc)         $entity = $this->onBeforePersistDisc($entity);
        if($entity instanceof Episode)      $entity = $this->onBeforePersistEpisode($entity);
        if($entity instanceof Organization) $entity = $this->onBeforePersistOrganization($entity);
        if($entity instanceof Post)         $entity = $this->onBeforePersistPost($entity);
        if($entity instanceof Season)       $entity = $this->onBeforePersistSeason($entity);
        if($entity instanceof Soundtrack)   $entity = $this->onBeforePersistSoundtrack($entity);
        if($entity instanceof User)         $entity = $this->onBeforePersistUser($entity);
        if($entity instanceof Volume)       $entity = $this->onBeforePersistVolume($entity);
    }

    /**
     * Configure the Ability before the manager persist it.
     *
     * @param Ability $ability
     * @return Ability
     */
    private function onBeforePersistAbility(Ability $ability): Ability {
        $ability->setSlug(strtolower($this->slugger->slug($ability->getName())));
        foreach($ability->getMedias() as $media) $this->manager->persist($media);
        return $ability;
    }

    /**
     * Configure the Arc before the manager persist it.
     *
     * @param Arc $arc
     * @return Arc
     */
    private function onBeforePersistArc(Arc $arc): Arc {
        $arc->setSlug(strtolower($this->slugger->slug($arc->getTitle())));
        if(!empty($arc->getCover())) $this->manager->persist($arc->getCover());
        foreach($arc->getChapters() as $chapter) {
            $chapter->setArc($arc);
            $this->manager->persist($chapter);
        }
        return $arc;
    }

    /**
     * Configure the Bluray before the manager persist it.
     *
     * @param Bluray $bluray
     * @return Bluray
     */
    private function onBeforePersistBluray(Bluray $bluray): Bluray {
        $bluray->setSlug(strtolower($this->slugger->slug($bluray->getTitle())));
        if(!empty($bluray->getCover())) $this->manager->persist($bluray->getCover());
        foreach($bluray->getEpisodes() as $episode) {
            $episode->setBluray($bluray);
            $this->manager->persist($episode);
        }
        return $bluray;
    }

    /**
     * Configure the Category before the manager persist it.
     *
     * @param Category $category
     * @return Category
     */
    private function onBeforePersistCategory(Category $category): Category {
        $category->setSlug(strtolower($this->slugger->slug($category->getTitle())));
        return $category;
    }

    /**
     * Configure the Chapter before the manager persist it.
     *
     * @param Chapter $chapter
     * @return Chapter
     */
    private function onBeforePersistChapter(Chapter $chapter): Chapter {
        $staff["script"] = (!empty($staff["script"])) ? $this->request->request->get("chapter")["script"] : "Koyoharu Gotouge";
        $staff["art"] = (!empty($staff["art"])) ? $this->request->request->get("chapter")["art"] : "Koyoharu Gotouge";
        $chapter->setStaff($staff);
        foreach($chapter->getMedias() as $media) $this->manager->persist($media);
        return $chapter;
    }

    /**
     * Configure the Character before the manager persist it.
     *
     * @param Character $character
     * @return Character
     */
    private function onBeforePersistCharacter(Character $character): Character {
        $character->setSlug(strtolower($this->slugger->slug($character->getName())));
        $relations = $character->getRelations();
        dd($relations);
        foreach($relations as $relation) {
            $rel = new Relation();
            $rel->setBase($relation->getRelative())
                ->setType($this->relationService->getInversedRelationType($character->getGender(), $relation->getType()))
                ->setRelative($relation->getBase())
            ;
            $this->manager->persist($rel);
        }
        return $character;
    }

    /**
     * Configure the Comment before the manager persist it.
     *
     * @param Comment $comment
     * @return Comment
     */
    private function onBeforePersistComment(Comment $comment): Comment {
        if(!$comment->getIsMuted()) {
            $user = $this->userRepository->findOneBy(["email" => $this->security->getUser()->getUserIdentifier()]);
            $comment
                ->setAuthor($user)
                ->setSentAt(new \DateTime("now"))
                ->setIsMuted(false)
            ;
        }
        return $comment;
    }

    /**
     * Configure the Disc before the manager persist it.
     *
     * @param Disc $disc
     * @return Disc
     */
    private function onBeforePersistDisc(Disc $disc): Disc {
        foreach($disc->getSoundtracks() as $soundtrack) {
            $soundtrack->setDisc($disc);
            $this->manager->persist($soundtrack);
        }
        return $disc;
    }

    /**
     * Configure the Episode before the manager persist it.
     *
     * @param Episode $episode
     * @return Episode
     */
    private function onBeforePersistEpisode(Episode $episode): Episode {
        $episode->setSlug(strtolower($this->slugger->slug($episode->getTitle())));
        $staff["directors"] = $this->request->request->get("episode")["directors"];
        $staff["storyboard"] = $this->request->request->get("episode")["storyboard"];
        $staff["animDirectors"] = $this->request->request->get("episode")["animDirectors"];
        $episode->setStaff($staff);
        foreach($episode->getChapters() as $chapter) {
            $chapter->addEpisode($episode);
            $this->manager->persist($chapter);
        }
        foreach($episode->getMedias() as $media) $this->manager->persist($media);
        return $episode;
    }

    /**
     * Configure the Organization before the manager persist it.
     *
     * @param Organization $organization
     * @return Organization
     */
    private function onBeforePersistOrganization(Organization $organization): Organization {
        $organization->setSlug(strtolower($this->slugger->slug($organization->getName())));
        return $organization;
    }

    /**
     * Configure the Post before the manager persist it.
     *
     * @param Post $post
     * @return Post
     */
    private function onBeforePersistPost(Post $post): Post {
        $post->setSlug(strtolower($this->slugger->slug($post->getTitle())));
        $user = $this->userRepository->findOneBy(["email" => $this->security->getUser()->getUserIdentifier()]);
        if($post->getAuthor() == null)
            $post
                ->setAuthor($user)
                ->setIsPublished(false)
                ->setPostedAt(new \DateTime("now"));
        else {
            $edit = new Edit();
            $edit
                ->setEditor($user)
                ->setEditedAt(new \DateTime("now"))
                ->setPost($post);
            $post->addEdit($edit);
            $this->manager->persist($edit);
        }
        $this->manager->persist($post->getCover());
        return $post;
    }

    /**
     * Configure the Season before the manager persist it.
     *
     * @param Season $season
     * @return Season
     */
    private function onBeforePersistSeason(Season $season): Season {
        foreach($season->getEpisodes() as $episode) {
            $episode->setSeason($season);
            $this->manager->persist($episode);
        }
        $this->manager->persist($season->getCover());
        return $season;
    }

    /**
     * Configure the Soundtrack before the manager persist it.
     *
     * @param Soundtrack $soundtrack
     * @return Soundtrack
     */
    private function onBeforePersistSoundtrack(Soundtrack $soundtrack): Soundtrack {
        $staff["vocals"] = $this->request->request->get("soundtrack")["vocals"];
        $staff["composer"] = $this->request->request->get("soundtrack")["composer"];
        $staff["arranger"] = $this->request->request->get("soundtrack")["arranger"];
        $staff["lyricist"] = $this->request->request->get("soundtrack")["lyricist"];
        $soundtrack->setStaff($staff);
        foreach($soundtrack->getEpisodes() as $episode) {
            $episode->addSoundtrack($soundtrack);
            $this->manager->persist($episode);
        }
        return $soundtrack;
    }

    /**
     * Configure the User before the manager persist it.
     *
     * @param User $user
     * @return User
     */
    private function onBeforePersistUser(User $user): User {
        return $user;
    }

    /**
     * Configure the Volume before the manager persist it.
     *
     * @param Volume $volume
     * @return Volume
     */
    private function onBeforePersistVolume(Volume $volume): Volume {
        foreach($volume->getChapters() as $chapter) {
            $chapter->addVolume($volume);
            $this->manager->persist($chapter);
        }
        $this->manager->persist($volume->getCover());
        return $volume;
    }
}