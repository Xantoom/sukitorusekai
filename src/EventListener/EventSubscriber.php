<?php

namespace App\EventListener;

use App\Entity\Relation;
use App\Repository\CharacterRepository;
use App\Repository\RelationRepository;
use App\Service\RelationService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class EventSubscriber implements EventSubscriberInterface{

    private EntityManagerInterface $manager;
    private RequestStack $requestStack;
    private CharacterRepository $characterRepository;
    private RelationRepository $relationRepository;

    public function __construct(EntityManagerInterface $manager, RequestStack $requestStack, CharacterRepository $characterRepository, RelationRepository $relationRepository) {
        $this->manager = $manager;
        $this->requestStack = $requestStack;
        $this->characterRepository = $characterRepository;
        $this->relationRepository = $relationRepository;
    }

    /**
     * Register all events
     * @return array
     */
    public function getSubscribedEvents(): array {
        return [
            Events::postRemove
        ];
    }

    /**
     * Event trigger after the manager remove an entity.
     * @param LifecycleEventArgs $event
     */
    public function postRemove(LifecycleEventArgs $event) {
        $entity = $event->getEntity();
        if($entity instanceof Relation) $this->deleteLinkedRelation($entity);
    }

    /**
     * Delete the linked Relation.
     * @param Relation $relation
     */
    private function deleteLinkedRelation(Relation $relation) {
        $request = $this->requestStack->getCurrentRequest();
        $character = $this->characterRepository->findOneBy(["name" => $request->request->get("Character")["name"]]);
        $linkRelation = $this->relationRepository->findOneBy(["base" => $relation->getRelative()->getId(), "relative" => $character->getId()]);
        if($linkRelation) {
            $this->manager->remove($linkRelation);
            $this->manager->flush();
        }
    }

}