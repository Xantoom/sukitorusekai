<?php

namespace App\Entity;

use App\Repository\EpisodeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EpisodeRepository::class)
 */
class Episode
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $romaji;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $number;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $duration;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $staff = [];

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $releasedAt;

    /**
     * @ORM\ManyToMany(targetEntity=Chapter::class, inversedBy="episodes", cascade={"persist"})
     */
    private $chapters;

    /**
     * @ORM\ManyToOne(targetEntity=Season::class, inversedBy="episodes", cascade={"persist"})
     */
    private $season;

    /**
     * @ORM\ManyToOne(targetEntity=Bluray::class, inversedBy="episodes", cascade={"persist"})
     */
    private $bluray;

    /**
     * @ORM\ManyToMany(targetEntity=Media::class)
     */
    private $medias;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity=Soundtrack::class, mappedBy="episodes")
     */
    private $soundtracks;

    public function __construct()
    {
        $this->chapters = new ArrayCollection();
        $this->medias = new ArrayCollection();
        $this->soundtracks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getRomaji(): ?string
    {
        return $this->romaji;
    }

    public function setRomaji(string $romaji): self
    {
        $this->romaji = $romaji;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getStaff(): ?array
    {
        return $this->staff;
    }

    public function setStaff(?array $staff): self
    {
        $this->staff = $staff;

        return $this;
    }

    public function getReleasedAt(): ?\DateTimeInterface
    {
        return $this->releasedAt;
    }

    public function setReleasedAt(?\DateTimeInterface $releasedAt): self
    {
        $this->releasedAt = $releasedAt;

        return $this;
    }

    /**
     * @return Collection|Chapter[]
     */
    public function getChapters(): Collection
    {
        return $this->chapters;
    }

    public function addChapter(Chapter $chapter): self
    {
        if (!$this->chapters->contains($chapter)) {
            $this->chapters[] = $chapter;
        }

        return $this;
    }

    public function removeChapter(Chapter $chapter): self
    {
        $this->chapters->removeElement($chapter);

        return $this;
    }

    public function getSeason(): ?Season
    {
        return $this->season;
    }

    public function setSeason(?Season $season): self
    {
        $this->season = $season;

        return $this;
    }

    public function hasSeason(): bool {
        if(!empty($this->getSeason())) return true;
        return false;
    }

    public function getBluray(): ?Bluray
    {
        return $this->bluray;
    }

    public function setBluray(?Bluray $bluray): self
    {
        $this->bluray = $bluray;

        return $this;
    }

    /**
     * @return Collection|Media[]
     */
    public function getMedias(): Collection
    {
        return $this->medias;
    }

    public function addMedia(Media $media): self
    {
        if (!$this->medias->contains($media)) {
            $this->medias[] = $media;
        }

        return $this;
    }

    public function removeMedia(Media $media): self
    {
        $this->medias->removeElement($media);

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Soundtrack[]
     */
    public function getSoundtracks(): Collection
    {
        return $this->soundtracks;
    }

    public function addSoundtrack(Soundtrack $soundtrack): self
    {
        if (!$this->soundtracks->contains($soundtrack)) {
            $this->soundtracks[] = $soundtrack;
            $soundtrack->addEpisode($this);
        }

        return $this;
    }

    public function removeSoundtrack(Soundtrack $soundtrack): self
    {
        if ($this->soundtracks->removeElement($soundtrack)) {
            $soundtrack->removeEpisode($this);
        }

        return $this;
    }

    public function __toString() {
        if($this->getSeason()) {
            if(!empty($this->number) && ($this->getSeason()->getNumber() != null))
                return "S" . $this->getSeason()->getNumber() . " - E" . $this->getNumber() . ": " . $this->getTitle();
        }
        return "Episode: " . $this->getTitle();
    }

    public function getDirectors(): string {
        return $this->getStaff()["directors"];
    }

    public function setDirectors(string $directors) {
        $this->staff["directors"] = $directors;
    }

    public function getAnimDirectors(): string {
        return $this->getStaff()["animDirectors"];
    }

    public function setAnimDirectors(string $animDirectors) {
        $this->staff["animDirectors"] = $animDirectors;
    }

    public function getStoryboard(): string {
        return $this->getStaff()["storyboard"];
    }

    public function setStoryboard(string $storyboard) {
        $this->staff["storyboard"] = $storyboard;
    }

    public function getCover(): ?Media {
        return $this->getMedias()[0];
    }

    public function setCover(Media $media) {
        $this->medias[0] = $media;
    }

    public function hasCover(): ?bool {
        return $this->getCover() != null;
    }

}
