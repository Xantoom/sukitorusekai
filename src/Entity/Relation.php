<?php

namespace App\Entity;

use App\Repository\RelationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RelationRepository::class)
 */
class Relation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Character::class, inversedBy="relations")
     */
    private $base;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=Character::class)
     */
    private $relative;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBase(): ?Character
    {
        return $this->base;
    }

    public function setBase(?Character $base): self
    {
        $this->base = $base;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getRelative(): ?Character
    {
        return $this->relative;
    }

    public function setRelative(?Character $relative): self
    {
        $this->relative = $relative;

        return $this;
    }
}
