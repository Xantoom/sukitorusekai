<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pseudo;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    /**
     * @ORM\Column(type="datetime")
     */
    private $registeredAt;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $about;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="author", cascade={"persist"})
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity=Post::class, mappedBy="author", cascade={"persist"})
     */
    private $posts;

    /**
     * @ORM\ManyToOne(targetEntity=Chapter::class, inversedBy="users", cascade={"persist"})
     */
    private $currentChapter;

    /**
     * @ORM\OneToMany(targetEntity=Post::class, mappedBy="editor", cascade={"persist"})
     */
    private $editedPosts;

    /**
     * @ORM\OneToMany(targetEntity=Edit::class, mappedBy="editor", cascade={"persist"})
     */
    private $edits;

    /**
     * @ORM\ManyToOne(targetEntity=Media::class, cascade={"persist"})
     */
    private $avatar;

    /**
     * @ORM\Column(type="boolean")
     */
    private $hasAvatar;

    public function __construct() {
        $this->roles = ["ROLE_USER"];
        $this->comments = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->registeredAt = new \DateTime();
        $this->editedPosts = new ArrayCollection();
        $this->edits = new ArrayCollection();
        $this->hasAvatar = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getRegisteredAt(): ?\DateTime
    {
        return $this->registeredAt;
    }

    public function setRegisteredAt(\DateTime $registeredAt): self
    {
        $this->registeredAt = $registeredAt;

        return $this;
    }

    public function getAbout(): ?string
    {
        return $this->about;
    }

    public function setAbout(?string $about): self
    {
        $this->about = $about;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setAuthor($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getAuthor() === $this) {
                $comment->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setAuthor($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->removeElement($post)) {
            // set the owning side to null (unless already changed)
            if ($post->getAuthor() === $this) {
                $post->setAuthor(null);
            }
        }

        return $this;
    }

    public function getCurrentChapter(): ?Chapter
    {
        return $this->currentChapter;
    }

    public function setCurrentChapter(?Chapter $currentChapter): self
    {
        $this->currentChapter = $currentChapter;

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getEditedPosts(): Collection
    {
        return $this->editedPosts;
    }

    public function addEditedPost(Post $editedPost): self
    {
        if (!$this->editedPosts->contains($editedPost)) {
            $this->editedPosts[] = $editedPost;
            $editedPost->setEditor($this);
        }

        return $this;
    }

    public function removeEditedPost(Post $editedPost): self
    {
        if ($this->editedPosts->removeElement($editedPost)) {
            // set the owning side to null (unless already changed)
            if ($editedPost->getEditor() === $this) {
                $editedPost->setEditor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Edit[]
     */
    public function getEdits(): Collection
    {
        return $this->edits;
    }

    public function addEdit(Edit $edit): self
    {
        if (!$this->edits->contains($edit)) {
            $this->edits[] = $edit;
            $edit->setEditor($this);
        }

        return $this;
    }

    public function removeEdit(Edit $edit): self
    {
        if ($this->edits->removeElement($edit)) {
            // set the owning side to null (unless already changed)
            if ($edit->getEditor() === $this) {
                $edit->setEditor(null);
            }
        }

        return $this;
    }

    public function getAvatar(): ?Media
    {
        return $this->avatar;
    }

    public function setAvatar(?Media $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getHasAvatar(): ?bool
    {
        return $this->hasAvatar;
    }

    public function setHasAvatar(bool $hasAvatar): self
    {
        $this->hasAvatar = $hasAvatar;

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }
}
