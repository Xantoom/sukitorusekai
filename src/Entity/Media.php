<?php

namespace App\Entity;

use App\Repository\MediaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=MediaRepository::class)
 * @Vich\Uploadable
 */
class Media {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $file;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="media_file", fileNameProperty="file")
     *
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTimeInterface|null
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=Character::class, inversedBy="medias")
     */
    private $knyCharacter;

    /**
     * @ORM\ManyToMany(targetEntity=Arc::class, inversedBy="medias")
     */
    private $arcs;

    /**
     * @ORM\ManyToMany(targetEntity=Bluray::class, inversedBy="medias")
     */
    private $blurays;

    /**
     * @ORM\ManyToMany(targetEntity=Volume::class, inversedBy="medias")
     */
    private $volumes;

    /**
     * @ORM\ManyToMany(targetEntity=Disc::class, inversedBy="medias")
     */
    private $discs;

    /**
     * @ORM\ManyToMany(targetEntity=Season::class, inversedBy="medias")
     */
    private $seasons;

    /**
     * @ORM\ManyToMany(targetEntity=Post::class, mappedBy="medias")
     */
    private $posts;

    public function __construct()
    {
        $this->characters = new ArrayCollection();
        $this->arcs = new ArrayCollection();
        $this->blurays = new ArrayCollection();
        $this->volumes = new ArrayCollection();
        $this->discs = new ArrayCollection();
        $this->seasons = new ArrayCollection();
        $this->posts = new ArrayCollection();
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): self {
        $this->imageFile = $imageFile;

        if ($imageFile)
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updateDateTime();
        return $this;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function updateDateTime() {
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getKnyCharacter(): ?Character
    {
        return $this->knyCharacter;
    }

    public function setKnyCharacter(?Character $knyCharacter): self
    {
        $this->knyCharacter = $knyCharacter;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|Arc[]
     */
    public function getArcs(): Collection
    {
        return $this->arcs;
    }

    public function addArc(Arc $arc): self
    {
        if (!$this->arcs->contains($arc)) {
            $this->arcs[] = $arc;
            $arc->addMedia($this);
        }

        return $this;
    }

    public function removeArc(Arc $arc): self
    {
        if ($this->arcs->removeElement($arc)) {
            $arc->removeMedia($this);
        }

        return $this;
    }

    /**
     * @return Collection|Bluray[]
     */
    public function getBlurays(): Collection
    {
        return $this->blurays;
    }

    public function addBluray(Bluray $bluray): self
    {
        if (!$this->blurays->contains($bluray)) {
            $this->blurays[] = $bluray;
            $bluray->addMedia($this);
        }

        return $this;
    }

    public function removeBluray(Bluray $bluray): self
    {
        if ($this->blurays->removeElement($bluray)) {
            $bluray->removeMedia($this);
        }

        return $this;
    }

    /**
     * @return Collection|Volume[]
     */
    public function getVolumes(): Collection
    {
        return $this->volumes;
    }

    public function addVolume(Volume $volume): self
    {
        if (!$this->volumes->contains($volume)) {
            $this->volumes[] = $volume;
            $volume->addMedia($this);
        }

        return $this;
    }

    public function removeVolume(Volume $volume): self
    {
        if ($this->volumes->removeElement($volume)) {
            $volume->removeMedia($this);
        }

        return $this;
    }

    /**
     * @return Collection|Disc[]
     */
    public function getDiscs(): Collection
    {
        return $this->discs;
    }

    public function addDisc(Disc $disc): self
    {
        if (!$this->discs->contains($disc)) {
            $this->discs[] = $disc;
            $disc->addMedia($this);
        }

        return $this;
    }

    public function removeDisc(Disc $disc): self
    {
        if ($this->discs->removeElement($disc)) {
            $disc->removeMedia($this);
        }

        return $this;
    }

    /**
     * @return Collection|Season[]
     */
    public function getSeasons(): Collection
    {
        return $this->seasons;
    }

    public function addSeason(Season $season): self
    {
        if (!$this->seasons->contains($season)) {
            $this->seasons[] = $season;
            $season->addMedia($this);
        }

        return $this;
    }

    public function removeSeason(Season $season): self
    {
        if ($this->seasons->removeElement($season)) {
            $season->removeMedia($this);
        }

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->addMedia($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->removeElement($post)) {
            $post->removeMedia($this);
        }

        return $this;
    }

}
