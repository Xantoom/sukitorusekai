<?php

namespace App\Entity;

use App\Repository\SoundtrackRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SoundtrackRepository::class)
 */
class Soundtrack
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $romaji;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $duration;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $staff = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $number;

    /**
     * @ORM\ManyToOne(targetEntity=Disc::class, inversedBy="soundtracks")
     */
    private $disc;

    /**
     * @ORM\ManyToMany(targetEntity=Episode::class, inversedBy="soundtracks")
     */
    private $episodes;

    public function __construct()
    {
        $this->episodes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getRomaji(): ?string
    {
        return $this->romaji;
    }

    public function setRomaji(string $romaji): self
    {
        $this->romaji = $romaji;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getStaff(): ?array
    {
        return $this->staff;
    }

    public function setStaff(?array $staff): self
    {
        $this->staff = $staff;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getDisc(): ?Disc
    {
        return $this->disc;
    }

    public function setDisc(?Disc $disc): self
    {
        $this->disc = $disc;

        return $this;
    }

    /**
     * @return Collection|Episode[]
     */
    public function getEpisodes(): Collection
    {
        return $this->episodes;
    }

    public function addEpisode(Episode $episode): self
    {
        if (!$this->episodes->contains($episode)) {
            $this->episodes[] = $episode;
        }

        return $this;
    }

    public function removeEpisode(Episode $episode): self
    {
        $this->episodes->removeElement($episode);

        return $this;
    }

    public function getVocals(): string {
        return $this->getStaff()["vocals"];
    }

    public function setVocals(string $vocals) {
        $this->staff["vocals"] = $vocals;
    }

    public function getComposer(): string {
        return $this->getStaff()["composer"];
    }

    public function setComposer(string $composer) {
        $this->staff["composer"] = $composer;
    }

    public function getArranger(): string {
        return $this->getStaff()["arranger"];
    }

    public function setArranger(string $arranger) {
        $this->staff["arranger"] = $arranger;
    }

    public function getLyricist(): string {
        return $this->getStaff()["lyricist"];
    }

    public function setLyricist(string $lyricist) {
        $this->staff["lyricist"] = $lyricist;
    }

    public function __toString() {
        return "Track " . $this->getNumber() . ": " . $this->getTitle();
    }


}
