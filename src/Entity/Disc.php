<?php

namespace App\Entity;

use App\Repository\DiskRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DiskRepository::class)
 */
class Disc
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $romaji;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $releasedAt;

    /**
     * @ORM\OneToMany(targetEntity=Soundtrack::class, mappedBy="disk", cascade={"persist"})
     */
    private $soundtracks;

    /**
     * @ORM\ManyToMany(targetEntity=Media::class, inversedBy="discs")
     */
    private $medias;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    public function __construct()
    {
        $this->soundtracks = new ArrayCollection();
        $this->medias = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getRomaji(): ?string
    {
        return $this->romaji;
    }

    public function setRomaji(string $romaji): self
    {
        $this->romaji = $romaji;

        return $this;
    }

    public function getReleasedAt(): ?\DateTimeInterface
    {
        return $this->releasedAt;
    }

    public function setReleasedAt(?\DateTimeInterface $releasedAt): self
    {
        $this->releasedAt = $releasedAt;

        return $this;
    }

    /**
     * @return Collection|Soundtrack[]
     */
    public function getSoundtracks(): Collection
    {
        return $this->soundtracks;
    }

    public function addSoundtrack(Soundtrack $soundtrack): self
    {
        if (!$this->soundtracks->contains($soundtrack)) {
            $this->soundtracks[] = $soundtrack;
            $soundtrack->setDisc($this);
        }

        return $this;
    }

    public function removeSoundtrack(Soundtrack $soundtrack): self
    {
        if ($this->soundtracks->removeElement($soundtrack)) {
            // set the owning side to null (unless already changed)
            if ($soundtrack->getDisc() === $this) {
                $soundtrack->setDisc(null);
            }
        }

        return $this;
    }

    public function getSoundtrackAmount(): int {
        return count($this->getSoundtracks());
    }

    public function __toString() {
        return $this->getTitle();
    }

    /**
     * @return Collection|Media[]
     */
    public function getMedias(): Collection
    {
        return $this->medias;
    }

    public function addMedia(Media $media): self
    {
        if (!$this->medias->contains($media)) {
            $this->medias[] = $media;
        }

        return $this;
    }

    public function removeMedia(Media $media): self
    {
        $this->medias->removeElement($media);

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCover(): ?Media {
        return $this->getMedias()[0];
    }

    public function setCover(Media $media) {
        $this->medias[0] = $media;
    }

    public function hasCover(): ?bool {
        return $this->getCover() != null;
    }
}
