<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use App\Repository\PostRepository;
use App\Service\CommentService;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("posts", name="post")
 */
class PostController extends AbstractController {

    protected PostRepository $postRepository;
    protected CommentRepository $commentRepository;
    protected CommentService $commentService;

    public function __construct(PostRepository $postRepository, CommentRepository $commentRepository, CommentService $commentService) {
        $this->postRepository = $postRepository;
        $this->commentRepository = $commentRepository;
        $this->commentService = $commentService;
    }

    /**
     * @Route("", name="_showcase")
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function showcase(PaginatorInterface $paginator, Request $request): Response {
        $posts = array_reverse($this->postRepository->findAll());
        $posts = $paginator->paginate($posts, $request->query->getInt("page", 1), 12);
        return $this->render("post/showcase.html.twig", [
            "posts" => $posts
        ]);
    }

    /**
     * @Route("/{slug}", name="_show", requirements={"slug"="^[a-z0-9]+(?:-[a-z0-9]+)*$"})
     * @param $slug
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function show($slug, Request $request, EntityManagerInterface $manager, PaginatorInterface $paginator): Response {
        $post = $this->postRepository->findOneBy(["slug" => $slug]);
        if(!$post) {
            $this->addFlash("error", "This post doesn't exist.");
            return $this->redirectToRoute("post_showcase");
        }
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            if($this->commentService->checkComment($comment, $post) == false)
                return $this->redirectToRoute("post_show", ["slug" => $slug]);
            $comment->setPost($post);
            $manager->persist($comment);
            $post->addComment($comment);
            $manager->persist($post);
            $manager->flush();
            $this->addFlash("success", "Your comment has been successfully added.");
            return $this->redirectToRoute("post_show", ["slug" => $slug]);
        }
        $comments = $paginator->paginate(array_reverse($post->getComments()->toArray()), $request->query->getInt("page", 1), 10);
        return $this->render("post/show.html.twig", [
            "post" => $post,
            "comments" => $comments,
            "form" => $form->createView()
        ]);
    }

}