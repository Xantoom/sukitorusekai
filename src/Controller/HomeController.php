<?php

namespace App\Controller;

use App\Repository\PostRepository;
use App\Service\CharacterService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController {

    /**
     * @Route("/", name="home")
     */
    public function index(PostRepository $postRepository, CharacterService $characterService): Response {
        return $this->render('home/index.html.twig', [
            "posts" => $postRepository->getLasts(4),
            "characters" => $characterService->getCharactersByViews(6),
        ]);
    }
}
