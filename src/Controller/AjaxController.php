<?php

namespace App\Controller;

use App\Entity\Comment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("ajax", name="ajax")
 */
class AjaxController extends AbstractController {

    protected EntityManagerInterface $manager;

    public function __construct(EntityManagerInterface $manager) {
        $this->manager = $manager;
    }

    /**
     * @Route("/hide/comment/{id}", name="_hide_comment")
     */
    public function hideComment(Comment $comment) {
        $comment->setIsMuted(true);
        $this->manager->persist($comment);
        $this->manager->flush();
    }

    /**
     * @Route("/show/comment/{id}", name="_show_comment")
     */
    public function showComment(Comment $comment) {
        $comment->setIsMuted(false);
        $this->manager->persist($comment);
        $this->manager->flush();
    }

}