<?php

namespace App\Controller;

use App\Repository\CharacterRepository;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("character", name="character")
 */
class CharacterController extends AbstractController {

    protected CharacterRepository $characterRepository;
    protected UserService $userService;

    public function __construct(CharacterRepository $characterRepository, UserService $userService) {
        $this->characterRepository = $characterRepository;
        $this->userService = $userService;
    }

    /**
     * @Route("", name="_showcase")
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function showcase(PaginatorInterface $paginator, Request $request): Response {
        $characters = array_reverse($this->characterRepository->findAll());
        $characters = $paginator->paginate($characters, $request->query->getInt("page", 1), 12);
        return $this->render("character/showcase.html.twig", [
            "characters" => $characters
        ]);
    }

    /**
     * @Route("/{slug}", name="_show", requirements={"slug"="^[a-z0-9]+(?:-[a-z0-9]+)*$"})
     * @param $slug
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function show($slug, Request $request, EntityManagerInterface $manager): Response {
        $character = $this->characterRepository->findOneBy(["slug" => $slug]);
        if(!$character) {
            $this->addFlash("error", "This character doesn't exist.");
            return $this->redirectToRoute("character_showcase");
        }
        $user = $this->userService->getCurrentUser();
        if($user && !(str_contains($request->server->get("HTTP_REFERER"), "warning")) ) {
            if($user->getCurrentChapter()->getNumber() < $character->getFirstChapterAppearance()->getNumber())
                return $this->redirectToRoute("warning_spoiler", [
                    "page" => "character",
                    "id" => $character->getId()
                ]);
            if(!empty($character->getAbilities())) {
                foreach($character->getAbilities() as $ability) {
                    if($ability->getSpoilChapter()->getNumber() > $user->getCurrentChapter()->getNumber())
                        return $this->redirectToRoute("warning_spoiler", [
                            "page" => "character",
                            "id" => $character->getId()
                        ]);
                }
            }
        }
        $character->addView(1);
        $manager->persist($character);
        $manager->flush();
        return $this->render("character/show.html.twig", [
            "character" => $character,
        ]);
    }

}