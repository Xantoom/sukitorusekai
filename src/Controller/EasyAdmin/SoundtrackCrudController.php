<?php

namespace App\Controller\EasyAdmin;

use App\Entity\Soundtrack;
use App\Form\MediaType;
use App\Service\PersistService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class SoundtrackCrudController extends AbstractCrudController {

    private PersistService $persistService;

    public function __construct(PersistService $persistService) {
        $this->persistService = $persistService;
    }

    public static function getEntityFqcn(): string {
        return Soundtrack::class;
    }

    public function configureFields(string $pageName): iterable {
        return [
            TextField::new("title"),
            TextField::new("romaji")->hideOnIndex(),
            NumberField::new("number"),
            NumberField::new("duration")->setHelp("In seconds"),
            TextField::new("vocals")->hideOnIndex(),
            TextField::new("composer"),
            TextField::new("arranger")->hideOnIndex(),
            TextField::new("lyricist")->hideOnIndex(),
            AssociationField::new("disc"),
            AssociationField::new("episodes"),
        ];
    }

    public function configureCrud(Crud $crud): Crud {
        return $crud->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void {
        $entityInstance = $this->persistService->configure($entityInstance);
        parent::persistEntity($entityManager, $entityInstance);

    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void {
        $entityInstance = $this->persistService->configure($entityInstance);
        parent::updateEntity($entityManager, $entityInstance);
    }
}