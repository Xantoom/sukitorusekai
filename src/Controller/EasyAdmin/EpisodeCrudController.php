<?php

namespace App\Controller\EasyAdmin;

use App\Entity\Episode;
use App\Form\MediaType;
use App\Service\PersistService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class EpisodeCrudController extends AbstractCrudController {

    private PersistService $persistService;

    public function __construct(PersistService $persistService) {
        $this->persistService = $persistService;
    }

    public static function getEntityFqcn(): string {
        return Episode::class;
    }

    public function configureFields(string $pageName): iterable {
        return [
            TextField::new("title"),
            TextField::new("romaji")->hideOnIndex(),
            NumberField::new("number"),
            NumberField::new("duration")->setHelp("In seconds")->hideOnIndex(),
            TextField::new("directors")->hideOnIndex(),
            TextField::new("storyboard")->hideOnIndex(),
            TextField::new("animDirectors")->hideOnIndex(),
            DateField::new("releasedAt"),
            TextEditorField::new("description")->setFormType(CKEditorType::class),
            AssociationField::new("chapters")->hideOnIndex(),
            AssociationField::new("season"),
            AssociationField::new("bluray")->hideOnIndex(),
            CollectionField::new("medias")->setEntryType(MediaType::class)
        ];
    }

    public function configureCrud(Crud $crud): Crud {
        return $crud->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void {
        $entityInstance = $this->persistService->configure($entityInstance);
        parent::persistEntity($entityManager, $entityInstance);

    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void {
        $entityInstance = $this->persistService->configure($entityInstance);
        parent::updateEntity($entityManager, $entityInstance);
    }
}