<?php

namespace App\Controller\EasyAdmin;

use App\Entity\Ability;
use App\Service\PersistService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class AbilityCrudController extends AbstractCrudController {

    private PersistService $persistService;

    public function __construct(PersistService $persistService) {
        $this->persistService = $persistService;
    }

    public static function getEntityFqcn(): string {
        return Ability::class;
    }

    public function configureFields(string $pageName): iterable {
        return [
            TextField::new("name"),
            TextField::new("romaji"),
            TextEditorField::new("summary")->setFormType(CKEditorType::class),
            TextEditorField::new("description")->setFormType(CKEditorType::class)->hideOnIndex(),
            AssociationField::new("spoilChapter"),
        ];
    }

    public function configureCrud(Crud $crud): Crud {
        return $crud->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void {
        $entityInstance = $this->persistService->configure($entityInstance);
        parent::persistEntity($entityManager, $entityInstance);

    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void {
        $entityInstance = $this->persistService->configure($entityInstance);
        parent::updateEntity($entityManager, $entityInstance);
    }
}