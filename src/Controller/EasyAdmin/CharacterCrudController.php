<?php

namespace App\Controller\EasyAdmin;

use App\Entity\Character;
use App\Entity\Relation;
use App\Form\admin\AdminAbilityType;
use App\Form\admin\AdminOrganizationType;
use App\Form\MediaType;
use App\Form\RelationType;
use App\Service\PersistService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CharacterCrudController extends AbstractCrudController {

    private PersistService $persistService;

    public function __construct(PersistService $persistService) {
        $this->persistService = $persistService;
    }

    public static function getEntityFqcn(): string {
        return Character::class;
    }

    public function configureFields(string $pageName): iterable {
        return [
            TextField::new("name"),
            TextField::new("aliases")->hideOnIndex(),
            IntegerField::new("age")->hideOnIndex(),
            IntegerField::new("height")->hideOnIndex(),
            IntegerField::new("weight")->hideOnIndex(),
            TextField::new("gender")->hideOnIndex(),
            TextField::new("race")->hideOnIndex(),
            TextField::new("voiceActor")->hideOnIndex(),
            TextEditorField::new("description")->hideOnIndex(),
            CollectionField::new("relations")->setEntryType(RelationType::class),
            AssociationField::new("abilities")->hideOnIndex(),
            AssociationField::new("organizations"),
            AssociationField::new("firstChapterAppearance")->hideOnIndex(),
            CollectionField::new("medias")->setEntryType(MediaType::class)->onlyOnForms(),
            IntegerField::new("views")->onlyOnIndex()
        ];
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void {
        $entityInstance = $this->persistService->configure($entityInstance);
        parent::persistEntity($entityManager, $entityInstance);

    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void {
        $entityInstance = $this->persistService->configure($entityInstance);
        parent::updateEntity($entityManager, $entityInstance);
    }

}