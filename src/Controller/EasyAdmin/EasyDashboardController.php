<?php

namespace App\Controller\EasyAdmin;

use App\Entity\Ability;
use App\Entity\Arc;
use App\Entity\Bluray;
use App\Entity\Category;
use App\Entity\Chapter;
use App\Entity\Character;
use App\Entity\Disc;
use App\Entity\Episode;
use App\Entity\Organization;
use App\Entity\Post;
use App\Entity\Season;
use App\Entity\Soundtrack;
use App\Entity\Volume;
use App\Service\UserService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class EasyDashboardController extends AbstractDashboardController {

    protected UserService $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    /**
     * @Route("/easyadmin", name="easyadmin")
     */
    public function index(): Response {
        return parent::index();
    }

    /**
     * Menu of admin dashboard.
     * @return iterable
     */
    public function configureMenuItems(): iterable {
        yield MenuItem::linktoDashboard('Dashboard','ph-house');
        yield MenuItem::linkToCrud('Abilities',     'ph-sword',         Ability::class);
        yield MenuItem::linkToCrud('Arcs',          'ph-bookmarks',     Arc::class);
        yield MenuItem::linkToCrud('Blurays',       'ph-disc',          Bluray::class);
        yield MenuItem::linkToCrud('Categories',    'ph-folder',        Category::class);
        yield MenuItem::linkToCrud('Chapters',      'ph-book-open',     Chapter::class);
        yield MenuItem::linkToCrud('Characters',    'ph-person',        Character::class);
        yield MenuItem::linkToCrud('Discs',         'ph-record',        Disc::class);
        yield MenuItem::linkToCrud('Episodes',      'ph-monitor-play',  Episode::class);
        yield MenuItem::linkToCrud('Organizations', 'ph-users-three',   Organization::class);
        yield MenuItem::linkToCrud('Posts',         'ph-article',       Post::class);
        yield MenuItem::linkToCrud('Seasons',       'ph-television',    Season::class);
        yield MenuItem::linkToCrud('Soundtracks',   'ph-music-note',    Soundtrack::class);
        yield MenuItem::linkToCrud('Volumes',       'ph-books',         Volume::class);
    }

    /**
     * Add Phosphor icons to the <head>
     * @return Assets
     */
    public function configureAssets(): Assets {
        return parent::configureAssets()
            ->addHtmlContentToHead("<script src=\"https://unpkg.com/phosphor-icons\"></script>");
    }

    /**
     * Configure the user interface in the admin dashboard.
     * @param UserInterface $user
     * @return UserMenu
     */
    public function configureUserMenu(UserInterface $user): UserMenu {
        $user = $this->userService->getCurrentUser();
        return parent::configureUserMenu($user)
            ->setName($this->userService->getCurrentUser()->getPseudo())
            ->setAvatarUrl($user->getHasAvatar() ? $user->getAvatar()->getImageFile() : null);
    }

    /**
     * Config of the admin dashboard.
     * @return Dashboard
     */
    public function configureDashboard(): Dashboard {
        return Dashboard::new()
            ->setTitle('SukitoruSekai')
            ->setFaviconPath("img/logo.svg");
    }
}
