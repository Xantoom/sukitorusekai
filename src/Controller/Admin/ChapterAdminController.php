<?php

namespace App\Controller\Admin;

use App\Entity\Chapter;
use App\Form\ChapterType;
use App\Repository\ChapterRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/chapter", name="admin_chapter")
 */
class ChapterAdminController extends AbstractController {

    protected ChapterRepository $chapterRepository;
    protected EntityManagerInterface $manager;

    public function __construct(ChapterRepository $chapterRepository, EntityManagerInterface $manager) {
        $this->chapterRepository = $chapterRepository;
        $this->manager = $manager;
    }

    /**
     * @Route("/", name="_table")
     */
    public function table(PaginatorInterface $paginator, Request $request): Response {
        $data = array_reverse($this->chapterRepository->findAll());
        $chapters = $paginator->paginate($data, $request->query->getInt("page", 1), 10);
        return $this->render('admin/chapter/table.html.twig', [
            "chapters" => $chapters
        ]);
    }

    /**
     * @Route("/add", name="_add")
     */
    public function add(Request $request): Response {
        $chapter = new Chapter();
        $form = $this->createForm(ChapterType::class, $chapter);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $dbChapter = $this->chapterRepository->findBy(["title" => $chapter->getTitle()]);
            if(!empty($dbChapter)) {
                $this->addFlash("error", "The arc with the title " . $chapter->getTitle() . " already exist.");
                return $this->redirectToRoute("admin_chapter_add");
            }
            $this->manager->persist($chapter);
            $this->manager->flush();
            $this->addFlash("success", "Chapter \"" . $chapter->getTitle() . "\"  has successfully been created in the database.");
            return $this->redirectToRoute("admin_chapter_table");
        }

        return $this->render("admin/chapter/add.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="_edit", requirements={"id"="\d+"})
     */
    public function edit(?Chapter $chapter, Request $request): Response {
        $form = $this->createForm(ChapterType::class, $chapter);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($chapter);
            $this->manager->flush();
            $this->addFlash("success", "Chapter \"" . $chapter->getTitle() . "\" has successfully been updated.");
            return $this->redirectToRoute("admin_chapter_table");
        }
        $form->get("script")->setData($chapter->getStaff()["script"]);
        $form->get("art")->setData($chapter->getStaff()["art"]);
        return $this->render("admin/chapter/edit.html.twig", [
            "form" => $form->createView()
        ]);
    }

}