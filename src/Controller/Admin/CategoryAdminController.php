<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/category", name="admin_category")
 */
class CategoryAdminController extends AbstractController {

    protected CategoryRepository $categoryRepository;
    protected EntityManagerInterface $em;

    public function __construct(CategoryRepository $categoryRepository, EntityManagerInterface $em) {
        $this->categoryRepository = $categoryRepository;
        $this->em = $em;
    }

    /**
     * @Route("/", name="_table")
     */
    public function table(PaginatorInterface $paginator, Request $request): Response {
        $data = array_reverse($this->categoryRepository->findAll());
        $categories = $paginator->paginate($data, $request->query->getInt("page", 1), 10);
        return $this->render('admin/category/table.html.twig', [
            "categories" => $categories
        ]);
    }

    /**
     * @Route("/add", name="_add")
     */
    public function add(Request $request): Response {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $dbCategory = $this->categoryRepository->findBy(["title" => $category->getTitle()]);
            if(!empty($dbCategory)) {
                $this->addFlash("error", "Category with the title " . $category->getTitle() . " already exist.");
                return $this->redirectToRoute("admin_category_add");
            }
            $this->em->persist($category);
            $this->em->flush();
            $this->addFlash("success", "Category \"" . $category->getTitle() . "\" has successfully been created in the database.");
            return $this->redirectToRoute("admin_category_table");
        }

        return $this->render("admin/category/add.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="_edit", requirements={"id"="\d+"})
     */
    public function edit(?Category $category, Request $request): Response {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($category);
            $this->em->flush();
            $this->addFlash("success", "Category \"" . $category->getTitle() . "\" has successfully been updated.");
            return $this->redirectToRoute("admin_category_table");
        }
        return $this->render("admin/category/edit.html.twig", [
            "form" => $form->createView()
        ]);
    }
}
