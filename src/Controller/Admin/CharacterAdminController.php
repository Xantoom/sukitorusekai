<?php

namespace App\Controller\Admin;

use App\Entity\Character;
use App\Form\CharacterType;
use App\Repository\CharacterRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/character", name="admin_character")
 */
class CharacterAdminController extends AbstractController {

    protected CharacterRepository $characterRepository;
    protected EntityManagerInterface $manager;

    public function __construct(CharacterRepository $characterRepository, EntityManagerInterface $manager) {
        $this->characterRepository = $characterRepository;
        $this->manager = $manager;
    }

    /**
     * @Route("/", name="_table")
     */
    public function table(PaginatorInterface $paginator, Request $request): Response {
        $data = array_reverse($this->characterRepository->findAll());
        $characters = $paginator->paginate($data, $request->query->getInt("page", 1), 10);
        return $this->render('admin/character/table.html.twig', [
            "characters" => $characters
        ]);
    }

    /**
     * @Route("/add", name="_add")
     */
    public function add(Request $request): Response {
        $character = new Character();
        $form = $this->createForm(CharacterType::class, $character);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $dbCharacter = $this->characterRepository->findBy(["name" => $character->getName()]);
            if(!empty($dbCharacter)) {
                $this->addFlash("error", "Character with the title " . $character->getName() . " already exist.");
                return $this->redirectToRoute("admin_character_add");
            }
            $this->manager->persist($character);
            $this->manager->flush();
            $this->addFlash("success", "Character \"" . $character->getName() . "\" has successfully been created in the database.");
            return $this->redirectToRoute("admin_character_table");
        }

        return $this->render("admin/character/add.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="_edit", requirements={"id"="\d+"})
     */
    public function edit(?Character $character, Request $request): Response {
        $form = $this->createForm(CharacterType::class, $character);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($character);
            $this->manager->flush();
            $this->addFlash("success", "Character \"" . $character->getName() . "\" has successfully been updated.");
            return $this->redirectToRoute("admin_character_table");
        }
        return $this->render("admin/character/edit.html.twig", [
            "form" => $form->createView()
        ]);
    }

}