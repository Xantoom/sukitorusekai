<?php

namespace App\Controller\Admin;

use App\Entity\Arc;
use App\Form\ArcType;
use App\Repository\ArcRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/arc", name="admin_arc")
 */
class ArcAdminController extends AbstractController {

    protected ArcRepository $arcRepository;
    protected EntityManagerInterface $manager;

    public function __construct(ArcRepository $arcRepository, EntityManagerInterface $manager) {
        $this->arcRepository = $arcRepository;
        $this->manager = $manager;
    }

    /**
     * @Route("/", name="_table")
     */
    public function table(PaginatorInterface $paginator, Request $request): Response {
        $data = array_reverse($this->arcRepository->findAll());
        $arcs = $paginator->paginate($data, $request->query->getInt("page", 1), 10);
        return $this->render('admin/arc/table.html.twig', [
            "arcs" => $arcs
        ]);
    }

    /**
     * @Route("/add", name="_add")
     */
    public function add(Request $request): Response {
        $arc = new Arc();
        $form = $this->createForm(ArcType::class, $arc);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $dbArc = $this->arcRepository->findBy(["title" => $arc->getTitle()]);
            if(!empty($dbArc)) {
                $this->addFlash("error", "Arc with the title " . $arc->getTitle() . " already exist.");
                return $this->redirectToRoute("admin_arc_add");
            }
            $this->manager->persist($arc);
            $this->manager->flush();
            $this->addFlash("success", "Arc \"" . $arc->getTitle() . "\" has successfully been created in the database.");
            return $this->redirectToRoute("admin_arc_table");
        }

        return $this->render("admin/arc/add.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="_edit", requirements={"id"="\d+"})
     */
    public function edit(?Arc $arc, Request $request): Response {
        $form = $this->createForm(ArcType::class, $arc);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($arc);
            $this->manager->flush();
            $this->addFlash("success", "Arc \"" . $arc->getTitle() . "\" has successfully been updated.");
            return $this->redirectToRoute("admin_arc_table");
        }
        return $this->render("admin/arc/edit.html.twig", [
            "form" => $form->createView(),
            "arc" => $arc
        ]);
    }

}