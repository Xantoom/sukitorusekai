<?php

namespace App\Controller\Admin;

use App\Entity\Bluray;
use App\Form\BlurayType;
use App\Repository\BlurayRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/bluray", name="admin_bluray")
 */
class BlurayAdminController extends AbstractController {

    protected BlurayRepository $blurayRepository;
    protected EntityManagerInterface $manager;

    public function __construct(BlurayRepository $blurayRepository, EntityManagerInterface $manager) {
        $this->blurayRepository = $blurayRepository;
        $this->manager = $manager;
    }

    /**
     * @Route("/", name="_table")
     */
    public function table(PaginatorInterface $paginator, Request $request): Response {
        $data = array_reverse($this->blurayRepository->findAll());
        $blurays = $paginator->paginate($data, $request->query->getInt("page", 1), 10);
        return $this->render('admin/bluray/table.html.twig', [
            "blurays" => $blurays
        ]);
    }

    /**
     * @Route("/add", name="_add")
     */
    public function add(Request $request): Response {
        $bluray = new Bluray();
        $form = $this->createForm(BlurayType::class, $bluray);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $dbBluray = $this->blurayRepository->findBy(["title" => $bluray->getTitle()]);
            if(!empty($dbBluray)) {
                $this->addFlash("error", "Bluray with the title " . $bluray->getTitle() . " already exist.");
                return $this->redirectToRoute("admin_bluray_add");
            }
            $this->manager->persist($bluray);
            $this->manager->flush();
            $this->addFlash("success", "Bluray \"" . $bluray->getTitle() . "\" has successfully been created in the database.");
            return $this->redirectToRoute("admin_bluray_table");
        }

        return $this->render("admin/bluray/add.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="_edit", requirements={"id"="\d+"})
     */
    public function edit(?Bluray $bluray, Request $request): Response {
        $form = $this->createForm(BlurayType::class, $bluray);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($bluray);
            $this->manager->flush();
            $this->addFlash("success", "Bluray \"" . $bluray->getTitle() . "\" has successfully been updated.");
            return $this->redirectToRoute("admin_bluray_table");
        }
        return $this->render("admin/bluray/edit.html.twig", [
            "form" => $form->createView(),
            "bluray" => $bluray
        ]);
    }

}