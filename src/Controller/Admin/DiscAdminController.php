<?php

namespace App\Controller\Admin;

use App\Entity\Disc;
use App\Form\DiscType;
use App\Repository\DiscRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/disc", name="admin_disc")
 */
class DiscAdminController extends AbstractController {

    protected DiscRepository $discRepository;
    protected EntityManagerInterface $manager;

    public function __construct(DiscRepository $discRepository, EntityManagerInterface $manager) {
        $this->discRepository = $discRepository;
        $this->manager = $manager;
    }

    /**
     * @Route("/", name="_table")
     */
    public function table(PaginatorInterface $paginator, Request $request): Response {
        $data = array_reverse($this->discRepository->findAll());
        $discs = $paginator->paginate($data, $request->query->getInt("page", 1), 10);
        return $this->render('admin/disc/table.html.twig', [
            "discs" => $discs
        ]);
    }

    /**
     * @Route("/add", name="_add")
     */
    public function add(Request $request): Response {
        $disc = new Disc();
        $form = $this->createForm(DiscType::class, $disc);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $dbDisc = $this->discRepository->findBy(["title" => $disc->getTitle()]);
            if(!empty($dbDisc)) {
                $this->addFlash("error", "Disc with the title " . $disc->getTitle() . " already exist.");
                return $this->redirectToRoute("admin_disc_add");
            }
            $this->manager->persist($disc);
            $this->manager->flush();
            $this->addFlash("success", "Disc \"" . $disc->getTitle() . "\" has successfully been created in the database.");
            return $this->redirectToRoute("admin_disc_table");
        }

        return $this->render("admin/disc/add.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="_edit", requirements={"id"="\d+"})
     */
    public function edit(?Disc $disc, Request $request): Response {
        $form = $this->createForm(DiscType::class, $disc);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($disc);
            $this->manager->flush();
            $this->addFlash("success", "Disc \"" . $disc->getTitle() . "\" has successfully been updated.");
            return $this->redirectToRoute("admin_disc_table");
        }
        return $this->render("admin/disc/edit.html.twig", [
            "form" => $form->createView()
        ]);
    }

}