<?php

namespace App\Controller\Admin;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/post", name="admin_post")
 */
class PostAdminController extends AbstractController {

    protected PostRepository $postRepository;
    protected EntityManagerInterface $em;

    public function __construct(PostRepository $postRepository, EntityManagerInterface $em) {
        $this->postRepository = $postRepository;
        $this->em = $em;
    }

    /**
     * @Route("/", name="_table")
     */
    public function table(PaginatorInterface $paginator, Request $request): Response {
        $data = array_reverse($this->postRepository->findAll());
        $posts = $paginator->paginate($data, $request->query->getInt("page", 1), 10);
        return $this->render('admin/post/table.html.twig', [
            "posts" => $posts
        ]);
    }

    /**
     * @Route("/add", name="_add")
     */
    public function add(Request $request): Response {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $dbCategory = $this->postRepository->findBy(["title" => $post->getTitle()]);
            if(!empty($dbCategory)) {
                $this->addFlash("error", "The post with the title " . $post->getTitle() . " already exist.");
                return $this->redirectToRoute("admin_post_add");
            }
            $this->em->persist($post);
            $this->em->flush();
            $this->addFlash("success", "Post \"" . $post->getTitle() . "\" has successfully been created in the database.");
            return $this->redirectToRoute("admin_post_table");
        }

        return $this->render("admin/post/add.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="_edit", requirements={"id"="\d+"})
     */
    public function edit(?Post $post, Request $request): Response {
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($post);
            $this->em->flush();
            $this->addFlash("success", "Post \"" . $post->getTitle() . "\" has successfully been updated.");
            return $this->redirectToRoute("admin_post_table");
        }
        return $this->render("admin/post/edit.html.twig", [
            "form" => $form->createView(),
            "post" => $post
        ]);
    }

}