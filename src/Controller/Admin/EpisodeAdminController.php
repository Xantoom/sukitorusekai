<?php

namespace App\Controller\Admin;

use App\Entity\Episode;
use App\Form\EpisodeType;
use App\Repository\EpisodeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/episode", name="admin_episode")
 */
class EpisodeAdminController extends AbstractController {

    protected EpisodeRepository $episodeRepository;
    protected EntityManagerInterface $manager;

    public function __construct(EpisodeRepository $episodeRepository, EntityManagerInterface $manager) {
        $this->episodeRepository = $episodeRepository;
        $this->manager = $manager;
    }

    /**
     * @Route("/", name="_table")
     */
    public function table(PaginatorInterface $paginator, Request $request): Response {
        $data = array_reverse($this->episodeRepository->findAll());
        $episodes = $paginator->paginate($data, $request->query->getInt("page", 1), 10);
        return $this->render('admin/episode/table.html.twig', [
            "episodes" => $episodes
        ]);
    }

    /**
     * @Route("/add", name="_add")
     */
    public function add(Request $request): Response {
        $episode = new Episode();
        $form = $this->createForm(EpisodeType::class, $episode);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $dbEpisode = $this->episodeRepository->findBy(["title" => $episode->getTitle()]);
            if(!empty($dbEpisode)) {
                $this->addFlash("error", "Episode with the title " . $episode->getTitle() . " already exist.");
                return $this->redirectToRoute("admin_episode_add");
            }
            $this->manager->persist($episode);
            $this->manager->flush();
            $this->addFlash("success", "Episode \"" . $episode->getTitle() . "\" has successfully been created in the database.");
            return $this->redirectToRoute("admin_episode_table");
        }

        return $this->render("admin/episode/add.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="_edit", requirements={"id"="\d+"})
     */
    public function edit(?Episode $episode, Request $request): Response {
        $form = $this->createForm(EpisodeType::class, $episode);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($episode);
            $this->manager->flush();
            $this->addFlash("success", "Episode \"" . $episode->getTitle() . "\" has successfully been updated.");
            return $this->redirectToRoute("admin_episode_table");
        }
        $form->get("directors")->setData($episode->getStaff()["directors"]);
        $form->get("storyboard")->setData($episode->getStaff()["storyboard"]);
        $form->get("animDirectors")->setData($episode->getStaff()["animDirectors"]);
        $form->get("chapters")->setData($episode->getChapters());
        return $this->render("admin/episode/edit.html.twig", [
            "form" => $form->createView()
        ]);
    }

}