<?php

namespace App\Controller\Admin;

use App\Entity\Season;
use App\Form\SeasonType;
use App\Repository\SeasonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/season", name="admin_season")
 */
class SeasonAdminController extends AbstractController {

    protected SeasonRepository $seasonRepository;
    protected EntityManagerInterface $manager;

    public function __construct(SeasonRepository $seasonRepository, EntityManagerInterface $manager) {
        $this->seasonRepository = $seasonRepository;
        $this->manager = $manager;
    }

    /**
     * @Route("/", name="_table")
     */
    public function table(PaginatorInterface $paginator, Request $request): Response {
        $data = array_reverse($this->seasonRepository->findAll());
        $seasons = $paginator->paginate($data, $request->query->getInt("page", 1), 10);
        return $this->render('admin/season/table.html.twig', [
            "seasons" => $seasons
        ]);
    }

    /**
     * @Route("/add", name="_add")
     */
    public function add(Request $request): Response {
        $season = new Season();
        $form = $this->createForm(SeasonType::class, $season);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $dbSeason = $this->seasonRepository->findBy(["title" => $season->getTitle()]);
            if(!empty($dbSeason)) {
                $this->addFlash("error", "Season with the title " . $season->getTitle() . " already exist.");
                return $this->redirectToRoute("admin_season_add");
            }
            $this->manager->persist($season);
            $this->manager->flush();
            $this->addFlash("success", "Season \"" . $season->getTitle() . "\" has successfully been created in the database.");
            return $this->redirectToRoute("admin_season_table");
        }

        return $this->render("admin/season/add.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="_edit", requirements={"id"="\d+"})
     */
    public function edit(?Season $season, Request $request): Response {
        $form = $this->createForm(SeasonType::class, $season);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($season);
            $this->manager->flush();
            $this->addFlash("success", "Season \"" . $season->getTitle() . "\" has successfully been updated.");
            return $this->redirectToRoute("admin_season_table");
        }
        return $this->render("admin/season/edit.html.twig", [
            "form" => $form->createView(),
            "season" => $season
        ]);
    }

}