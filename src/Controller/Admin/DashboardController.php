<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin")
 */
class DashboardController extends AbstractController {

    /**
     * @Route("/", name="_dashboard")
     */
    public function index(): Response {
        return $this->render("admin/dashboard.html.twig");
    }
}
