<?php

namespace App\Controller\Admin;

use App\Entity\Organization;
use App\Form\OrganizationType;
use App\Repository\OrganizationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin_organization", name="admin_organization")
 */
class OrganizationAdminController extends AbstractController {

    protected OrganizationRepository $organizationRepository;
    protected EntityManagerInterface $manager;

    public function __construct(OrganizationRepository $organizationRepository, EntityManagerInterface $manager) {
        $this->organizationRepository = $organizationRepository;
        $this->manager = $manager;
    }

    /**
     * @Route("/", name="_table")
     */
    public function table(PaginatorInterface $paginator, Request $request): Response {
        $data = array_reverse($this->organizationRepository->findAll());
        $organizations = $paginator->paginate($data, $request->query->getInt("page", 1), 10);
        return $this->render('admin/organization/table.html.twig', [
            "organizations" => $organizations
        ]);
    }

    /**
     * @Route("/add", name="_add")
     */
    public function add(Request $request): Response {
        $organization = new Organization();
        $form = $this->createForm(OrganizationType::class, $organization);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $dbOrganization = $this->organizationRepository->findBy(["name" => $organization->getName()]);
            if(!empty($dbOrganization)) {
                $this->addFlash("error", "Organization with the name " . $organization->getName() . " already exist.");
                return $this->redirectToRoute("admin_organization_add");
            }
            $this->manager->persist($organization);
            $this->manager->flush();
            $this->addFlash("success", "Organization \"" . $organization->getName() . "\" has successfully been created in the database.");
            return $this->redirectToRoute("admin_organization_table");
        }

        return $this->render("admin/organization/add.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="_edit", requirements={"id"="\d+"})
     */
    public function edit(?Organization $organization, Request $request): Response {
        $form = $this->createForm(OrganizationType::class, $organization);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($organization);
            $this->manager->flush();
            $this->addFlash("success", "Organization \"" . $organization->getName() . "\" has successfully been updated.");
            return $this->redirectToRoute("admin_organization_table");
        }
        return $this->render("admin/organization/edit.html.twig", [
            "form" => $form->createView()
        ]);
    }

}