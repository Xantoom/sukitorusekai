<?php

namespace App\Controller\Admin;

use App\Entity\Volume;
use App\Form\VolumeType;
use App\Repository\VolumeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/volume", name="admin_volume")
 */
class VolumeAdminController extends AbstractController {

    protected VolumeRepository $volumeRepository;
    protected EntityManagerInterface $manager;

    public function __construct(VolumeRepository $volumeRepository, EntityManagerInterface $manager) {
        $this->volumeRepository = $volumeRepository;
        $this->manager = $manager;
    }

    /**
     * @Route("/", name="_table")
     */
    public function table(PaginatorInterface $paginator, Request $request): Response {
        $data = array_reverse($this->volumeRepository->findAll());
        $volumes = $paginator->paginate($data, $request->query->getInt("page", 1), 10);
        return $this->render('admin/volume/table.html.twig', [
            "volumes" => $volumes
        ]);
    }

    /**
     * @Route("/add", name="_add")
     */
    public function add(Request $request): Response {
        $volume = new Volume();
        $form = $this->createForm(VolumeType::class, $volume);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $dbVolume = $this->volumeRepository->findBy(["title" => $volume->getTitle()]);
            if(!empty($dbVolume)) {
                $this->addFlash("error", "Volume with the title " . $volume->getTitle() . " already exist.");
                return $this->redirectToRoute("admin_volume_add");
            }
            $this->manager->persist($volume);
            $this->manager->flush();
            $this->addFlash("success", "Volume \"" . $volume->getTitle() . "\" has successfully been created in the database.");
            return $this->redirectToRoute("admin_volume_table");
        }

        return $this->render("admin/volume/add.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="_edit", requirements={"id"="\d+"})
     */
    public function edit(?Volume $volume, Request $request): Response {
        $form = $this->createForm(VolumeType::class, $volume);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($volume);
            $this->manager->flush();
            $this->addFlash("success", "Volume \"" . $volume->getTitle() . "\" has successfully been updated.");
            return $this->redirectToRoute("admin_volume_table");
        }
        return $this->render("admin/volume/edit.html.twig", [
            "form" => $form->createView(),
            "volume" => $volume
        ]);
    }

}