<?php

namespace App\Controller\Admin;

use App\Entity\Ability;
use App\Form\AbilityType;
use App\Repository\AbilityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/ability", name="admin_ability")
 */
class AbilityAdminController extends AbstractController {

    protected AbilityRepository $abilityRepository;
    protected EntityManagerInterface $manager;

    public function __construct(AbilityRepository $abilityRepository, EntityManagerInterface $manager) {
        $this->abilityRepository = $abilityRepository;
        $this->manager = $manager;
    }

    /**
     * @Route("/", name="_table")
     */
    public function table(PaginatorInterface $paginator, Request $request): Response {
        $data = array_reverse($this->abilityRepository->findAll());
        $abilities = $paginator->paginate($data, $request->query->getInt("page", 1), 10);
        return $this->render('admin/ability/table.html.twig', [
            "abilities" => $abilities
        ]);
    }

    /**
     * @Route("/add", name="_add")
     */
    public function add(Request $request): Response {
        $ability = new Ability();
        $form = $this->createForm(AbilityType::class, $ability);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $dbAbility = $this->abilityRepository->findBy(["name" => $ability->getName()]);
            if(!empty($dbAbility)) {
                $this->addFlash("error", "Ability with the title " . $ability->getName() . " already exist.");
                return $this->redirectToRoute("admin_ability_add");
            }
            $this->manager->persist($ability);
            $this->manager->flush();
            $this->addFlash("success", "Ability \"" . $ability->getName() . "\" has successfully been created in the database.");
            return $this->redirectToRoute("admin_ability_table");
        }

        return $this->render("admin/ability/add.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="_edit", requirements={"id"="\d+"})
     */
    public function edit(?Ability $ability, Request $request) {
        if(!$ability) return $this->createNotFoundException();
        $form = $this->createForm(AbilityType::class, $ability);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($ability);
            $this->manager->flush();
            $this->addFlash("success", "Ability \"" . $ability->getName() . "\" has successfully been updated.");
            return $this->redirectToRoute("admin_ability_table");
        }
        return $this->render("admin/ability/edit.html.twig", [
            "form" => $form->createView(),
            "ability" => $ability
        ]);
    }

}