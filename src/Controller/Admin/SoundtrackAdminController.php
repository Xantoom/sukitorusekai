<?php

namespace App\Controller\Admin;

use App\Entity\Soundtrack;
use App\Form\SoundtrackType;
use App\Repository\SoundtrackRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/soundtrack", name="admin_soundtrack")
 */
class SoundtrackAdminController extends AbstractController {

    protected SoundtrackRepository $soundtrackRepository;
    protected EntityManagerInterface $manager;

    public function __construct(SoundtrackRepository $soundtrackRepository, EntityManagerInterface $manager) {
        $this->soundtrackRepository = $soundtrackRepository;
        $this->manager = $manager;
    }

    /**
     * @Route("/", name="_table")
     */
    public function table(PaginatorInterface $paginator, Request $request): Response {
        $data = array_reverse($this->soundtrackRepository->findAll());
        $soundtracks = $paginator->paginate($data, $request->query->getInt("page", 1), 10);
        return $this->render('admin/soundtrack/table.html.twig', [
            "soundtracks" => $soundtracks
        ]);
    }

    /**
     * @Route("/add", name="_add")
     */
    public function add(Request $request): Response {
        $soundtrack = new Soundtrack();
        $form = $this->createForm(SoundtrackType::class, $soundtrack);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $dbSoundtrack = $this->soundtrackRepository->findBy(["title" => $soundtrack->getTitle()]);
            if(!empty($dbSoundtrack)) {
                $this->addFlash("error", "Soundtrack with the title " . $soundtrack->getTitle() . " already exist.");
                return $this->redirectToRoute("admin_soundtrack_add");
            }
            $this->manager->persist($soundtrack);
            $this->manager->flush();
            $this->addFlash("success", "Soundtrack \"" . $soundtrack->getTitle() . "\" has successfully been created in the database.");
            return $this->redirectToRoute("admin_soundtrack_table");
        }

        return $this->render("admin/soundtrack/add.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="_edit", requirements={"id"="\d+"})
     */
    public function edit(?Soundtrack $soundtrack, Request $request): Response {
        $form = $this->createForm(SoundtrackType::class, $soundtrack);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($soundtrack);
            $this->manager->flush();
            $this->addFlash("success", "Soundtrack \"" . $soundtrack->getTitle() . "\" has successfully been updated.");
            return $this->redirectToRoute("admin_soundtrack_table");
        }
        $form->get("vocals")->setData($soundtrack->getStaff()["vocals"]);
        $form->get("lyricist")->setData($soundtrack->getStaff()["lyricist"]);
        $form->get("arranger")->setData($soundtrack->getStaff()["arranger"]);
        $form->get("composer")->setData($soundtrack->getStaff()["composer"]);
        return $this->render("admin/soundtrack/edit.html.twig", [
            "form" => $form->createView()
        ]);
    }

}