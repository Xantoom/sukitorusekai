<?php

namespace App\Controller;

use App\Form\SpoilerType;
use App\Repository\CharacterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("warning", name="warning")
 */
class WarningController extends AbstractController {

    protected CharacterRepository $characterRepository;

    public function __construct(CharacterRepository $characterRepository) {
        $this->characterRepository = $characterRepository;
    }

    /**
     * @Route("/{page}/{id}", name="_spoiler", requirements={"id"="\d+"})
     */
    public function spoiler($page, $id, Request $request, RequestStack $requestStack) {
        $form = $this->createForm(SpoilerType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            switch($page) {
                case "ability":
                    return 1;
                case "arc":
                    return 2;
                case "bluray":
                    return 3;
                case "chapter":
                    return 4;
                case "character":
                    $character = $this->characterRepository->find($id);
                    if(!$character) return $this->createNotFoundException();
                    return $this->redirectToRoute("character_show", ["slug" => $character->getSlug()]);
                case "disc":
                    return 5;
                case "episode":
                    return 6;
                case "organization":
                    return 7;
                case "season":
                    return 8;
                case "volume":
                    return 9;
                default: return $this->createNotFoundException();
            }
        }
        return $this->render("warning/spoiler.html.twig", [
            "form" => $form->createView()
        ]);
    }

}