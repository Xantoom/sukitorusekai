<?php

namespace App\DataFixtures;

use App\Entity\CD;
use App\Entity\Chapter;
use App\Entity\Soundtrack;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\String\Slugger\SluggerInterface;

class AppFixtures extends Fixture {

    protected SluggerInterface $slugger;

    public function __construct(SluggerInterface $slugger) {
        $this->slugger = $slugger;
    }

    public function load(ObjectManager $manager) {

        // FakerPhp
        $faker = Factory::create();
        // Slugger
        $slugger = $this->slugger;

        // Final Flush
        $manager->flush();
    }
}
