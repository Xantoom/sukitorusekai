<?php

namespace App\Twig;

use App\Repository\AbilityRepository;
use App\Repository\ArcRepository;
use App\Repository\OrganizationRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension {

    private AbilityRepository $abilityRepository;
    private ArcRepository $arcRepository;
    private OrganizationRepository $organizationRepository;

    public function __construct(AbilityRepository $abilityRepository, ArcRepository $arcRepository, OrganizationRepository $organizationRepository) {
        $this->abilityRepository = $abilityRepository;
        $this->arcRepository = $arcRepository;
        $this->organizationRepository = $organizationRepository;
    }

    /**
     * Create all new twig extensions.
     * @return TwigFunction[]
     */
    public function getFunctions(): array {
        return [
            new TwigFunction("abilities", [$this, "getAllAbilities"]),
            new TwigFunction("arcs", [$this, "getAllArcs"]),
            new TwigFunction("organizations", [$this, "getAllOrganizations"]),
        ];
    }

    /**
     * Return all abilities from the database.
     * @return array|null
     */
    public function getAllAbilities(): ?array {
        return $this->abilityRepository->findAll();
    }

    /**
     * Return all arcs from the database.
     * @return array|null
     */
    public function getAllArcs(): ?array {
        return $this->arcRepository->findAll();
    }

    /**
     * Return all organizations from the database.
     * @return array|null
     */
    public function getAllOrganizations(): ?array {
        return $this->organizationRepository->findAll();
    }

}
