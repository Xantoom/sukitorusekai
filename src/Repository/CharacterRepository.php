<?php

namespace App\Repository;

use App\Entity\Character;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Character|null find($id, $lockMode = null, $lockVersion = null)
 * @method Character|null findOneBy(array $criteria, array $orderBy = null)
 * @method Character[]    findAll()
 * @method Character[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CharacterRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Character::class);
    }

    /**
     * Return all characters sort by views
     * @return int|array|string
     */
    public function getAllCharactersByViews() {
        return $this
            ->createQueryBuilder('c')
            ->orderBy('c.views', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Return $maxCharacters characters sort by views
     * @param int $maxCharacters
     * @return int|mixed|string
     */
    public function getCharactersByViews(int $maxCharacters) {
        return $this
            ->createQueryBuilder('c')
            ->orderBy('c.views', 'DESC')
            ->setMaxResults($maxCharacters)
            ->getQuery()
            ->getResult();
    }

    /*
    public function findOneBySomeField($value): ?Character
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
