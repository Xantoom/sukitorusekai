<?php

namespace App\Form;

use App\Entity\Ability;
use App\Entity\Chapter;
use App\Entity\Character;
use App\Entity\Organization;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class CharacterType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, [
                "label" => "Name",
                "required" => true,
                'constraints' => [
                    new NotBlank(['message' => "Names must not be blank."]),
                ],
            ])
            ->add('aliases', TextType::class, [
                "label" => "Aliases",
                "required" => false,
            ])
            ->add('description', CKEditorType::class, [
                "label" => "Short Description",
                "attr" => [
                    "rows" => 16
                ],
                "config" => [
                    "height" => 200,
                    "resize_enabled" => false,
                    "removePlugins" => "exportpdf",
                ],
            ])
            ->add('age', NumberType::class, [
                "label" => "Age",
                "required" => true,
                "constraints" => [
                    new NotBlank(['message' => "Age must not be blank."])
                ]
            ])
            ->add('weight', NumberType::class, [
                "label" => "Weight",
                "required" => true,
                "constraints" => [
                    new NotBlank(['message' => "Weight must not be blank."])
                ]
            ])
            ->add('height', NumberType::class, [
                "label" => "Height",
                "required" => true,
                "constraints" => [
                    new NotBlank(['message' => "Height must not be blank."])
                ]
            ])
            ->add('gender', TextType::class, [
                "label" => "Gender",
                "required" => true,
                'constraints' => [
                    new NotBlank(['message' => "The gender must not be blank."]),
                ],
            ])
            ->add('race', TextType::class, [
                "label" => "Race",
                "required" => true,
                'constraints' => [
                    new NotBlank(['message' => "Race must not be blank."]),
                ],
            ])
            ->add('voiceActor', TextType::class, [
                "label" => "Voice Actor",
                "required" => true,
                'constraints' => [
                    new NotBlank(['message' => "Race must not be blank."]),
                ],
            ])
            ->add('relations', CollectionType::class, [
                "entry_type" => RelationType::class,
                'allow_add' => true,
                'allow_delete' => true,
                "by_reference" => false,
                "data" => $options["data"]->getRelations(),
            ])
            ->add('abilities', EntityType::class, [
                "required" => false,
                "label" => "Abilities",
                "multiple" => true,
                "class" => Ability::class,
                "data" => $options["data"]->getAbilities(),
                'choice_label' => function (?Ability $ability) {return $ability->getName();},
            ])
            ->add('organizations', EntityType::class, [
                "required" => false,
                "label" => "Organizations",
                "multiple" => true,
                "class" => Organization::class,
                "data" => $options["data"]->getOrganizations(),
                'choice_label' => function (?Organization $organization) {return $organization->getName();},
            ])
            ->add("cover", MediaType::class, [
                "label" => "Cover Picture",
            ])
            ->add("medias", CollectionType::class, [
                "entry_type" => MediaType::class,
                'label' => "Medias",
                'allow_add' => true,
                'allow_delete' => true,
                "by_reference" => false
            ])
            ->add("firstChapterAppearance", EntityType::class, [
                "required" => false,
                "label" => "First Chapter Appearance",
                "multiple" => false,
                "class" => Chapter::class,
                "data" => $options["data"]->getFirstChapterAppearance(),
                'choice_label' => function (?Chapter $chapter) {return "Chapter " . $chapter->getNumber();},
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Character::class,
        ]);
    }
}
