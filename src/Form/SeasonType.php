<?php

namespace App\Form;

use App\Entity\Episode;
use App\Entity\Season;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class SeasonType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('title', TextType::class, [
                "required" => true,
                "label" => "Title",
                "constraints" => [
                    new NotBlank(["message" => "The title must not be blank."])
                ]
            ])
            ->add('romaji', TextType::class, [
                "required" => true,
                "label" => "Romaji Title",
                "constraints" => [
                    new NotBlank(["message" => "The romaji title must not be blank."])
                ]
            ])
            ->add('cover', MediaType::class, [
                "label" => "Cover Picture",
            ])
            ->add("episodes", EntityType::class, [
                "required" => false,
                "label" => "Episodes",
                "multiple" => true,
                "class" => Episode::class,
                "data" => $options["data"]->getEpisodes(),
                'choice_label' => function (?Episode $episode) {return "Episode " . $episode->getNumber();},
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Season::class,
        ]);
    }
}
