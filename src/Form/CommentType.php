<?php

namespace App\Form;

use App\Entity\Comment;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class CommentType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('content', TextareaType::class, [
                "attr" => [
                    "placeholder" => "Write your comment here...",
                    "rows" => 2,
                    "cols" => 16
                ],
                "required" => true,
                "constraints" => [
                    new NotBlank(["message" => "Your message must not be blank."]),
                    new Length([
                        "min" => 2,
                        "minMessage" => "Your message needs to have at least 2 characters",
                        "max" => 1024,
                        "maxMessage" => "Your message should not exceed 1024 characters"
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
            "always_empty" => true
        ]);
    }
}
