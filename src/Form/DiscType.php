<?php

namespace App\Form;

use App\Entity\Disc;
use App\Entity\Soundtrack;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class DiscType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('title', TextType::class, [
                "required" => true,
                "label" => "Title",
                "constraints" => [
                    new NotBlank(["message" => "The title must not be blank."])
                ]
            ])
            ->add('romaji', TextType::class, [
                "required" => true,
                "label" => "Romaji Title",
                "constraints" => [
                    new NotBlank(["message" => "The romaji title must not be blank."])
                ]
            ])
            ->add('releasedAt', DateType::class, [
                "required" => false,
                "label" => "Release Date"
            ])
            ->add("cover", MediaType::class, [
                "label" => "Cover Picture"
            ])
            ->add("soundtracks", EntityType::class, [
                "required" => false,
                "label" => "Soundtracks",
                "multiple" => true,
                "class" => Soundtrack::class,
                "data" => $options["data"]->getSoundtracks(),
                'choice_label' => function (?Soundtrack $soundtrack) {return $soundtrack->getNumber() . ". " . $soundtrack->getTitle();},
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Disc::class,
        ]);
    }
}
