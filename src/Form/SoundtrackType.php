<?php

namespace App\Form;

use App\Entity\Chapter;
use App\Entity\Episode;
use App\Entity\Soundtrack;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class SoundtrackType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('title', TextType::class, [
                "required" => true,
                "label" => "Title",
                "constraints" => [
                    new NotBlank(["message" => "The title must not be blank."])
                ]
            ])
            ->add('romaji', TextType::class, [
                "required" => true,
                "label" => "Romaji Title",
                "constraints" => [
                    new NotBlank(["message" => "The romaji title must not be blank."])
                ]
            ])
            ->add('duration', NumberType::class, [
                "required" => true,
                "label" => "Duration (in seconds)",
            ])
            ->add('number', NumberType::class, [
                "required" => false,
                "label" => "Number",
            ])
            ->add('vocals', TextType::class, [
                "required" => false,
                "label" => "Vocals",
                "mapped" => false
            ])
            ->add("composer", TextType::class, [
                "required" => false,
                "label" => "Composer",
                "mapped" => false
            ])
            ->add("arranger", TextType::class, [
                "required" => false,
                "label" => "Arranger",
                "mapped" => false
            ])
            ->add("lyricist", TextType::class, [
                "required" => false,
                "label" => "Lyricist",
                "mapped" => false
            ])
            ->add("episodes", EntityType::class, [
                "required" => false,
                "label" => "Episodes",
                "multiple" => true,
                "class" => Episode::class,
                "data" => $options["data"]->getEpisodes(),
                'choice_label' => function (?Episode $episode) {return "Episode " . $episode->getNumber();},
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Soundtrack::class,
        ]);
    }
}
