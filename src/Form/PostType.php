<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Chapter;
use App\Entity\Media;
use App\Entity\Post;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PostType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add("title", TextType::class, [
                "label" => "Title",
                "required" => true,
                'constraints' => [
                    new NotBlank(['message' => "The title must not be blank."]),
                ],
            ])
            ->add("category", EntityType::class, [
                "class" => Category::class,
                "label" => "Category",
                'choice_label' => function ($category) {return $category->getTitle();},
                'choice_value' => function (?Category $entity) {return $entity ? $entity->getId() : '';},
                "required" => true,
            ])
            ->add("content", CKEditorType::class, [
                "label" => "Content",
                "attr" => [
                    "rows" => 16
                ],
                "config" => [
                    "height" => 400,
                    "resize_enabled" => false
                ],
            ])
            ->add("cover", MediaType::class, [
                "label" => "Cover Picture",
            ])
            ->add("spoilChapter", EntityType::class, [
                "class" => Chapter::class,
                "label" => "This post spoil the chapter:",
                "data" => $options["data"]->getSpoilChapter(),
                'choice_label' => function (?Chapter $chapter) {
                    if(!empty($chapter->getEpisodes())) {
                        $numbers = [];
                        foreach($chapter->getEpisodes() as $episode) $numbers[] = $episode->getNumber();
                        return "Chapter " . $chapter->getNumber() . " - Episode(s) " . implode("/", $numbers);
                    }
                    else return "Chapter " . $chapter->getNumber();
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
