<?php

namespace App\Form;

use App\Entity\Chapter;
use App\Entity\Organization;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class OrganizationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                "required" => true,
                "label" => "Name",
                "constraints" => [
                    new NotBlank(["message" => "The name must not be blank."])
                ]
            ])
            ->add('romaji', TextType::class, [
                "required" => true,
                "label" => "Romaji Name",
                "constraints" => [
                    new NotBlank(["message" => "The romaji name must not be blank."])
                ]
            ])
            ->add('chapterAppearance', EntityType::class, [
                "class" => Chapter::class,
                "label" => "This post spoil the chapter:",
                "data" => $options["data"]->getChapterAppearance(),
                'choice_label' => function (?Chapter $chapter) {
                    if(!empty($chapter->getEpisodes())) {
                        $numbers = [];
                        foreach($chapter->getEpisodes() as $episode) $numbers[] = $episode->getNumber();
                        return "Chapter " . $chapter->getNumber() . " - Episode(s) " . implode("/", $numbers);
                    }
                    else return "Chapter " . $chapter->getNumber();
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Organization::class,
        ]);
    }
}
