<?php

namespace App\Form;

use App\Entity\Chapter;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ChapterType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('title', TextType::class, [
                "label" => "Title",
                "required" => true,
                'constraints' => [
                    new NotBlank(['message' => "The title must not be blank."]),
                ],
            ])
            ->add('romaji', TextType::class, [
                "label" => "Romaji Title",
                "required" => true,
                'constraints' => [
                    new NotBlank(['message' => "The romaji title must not be blank."]),
                ],
            ])
            ->add('number', NumberType::class, [
                "label" => "Numero of the Chapter",
                "required" => false,
            ])
            ->add('pagesAmount', NumberType::class, [
                "label" => "Number of Pages",
                "required" => true,
                "constraints" => [
                    new NotBlank(['message' => "The amount of pages must not be blank."])
                ]
            ])
            ->add('releasedAt', DateType::class, [
                "label" => "Release Date",
                "required" => false
            ])
            ->add('script', TextType::class, [
                "mapped" => false,
                "label" => "Script by",
                "help" => "Koyoharu Gotouge if left empty",
                "required" => false,
            ])
            ->add('art', TextType::class, [
                "mapped" => false,
                "label" => "Art by",
                "help" => "Koyoharu Gotouge if left empty",
                "required" => false,
            ])
            ->add("description", CKEditorType::class, [
                "label" => "Content",
                "attr" => [
                    "rows" => 16
                ],
                "config" => [
                    "height" => 400,
                    "resize_enabled" => false
                ],
                "required" => false,
            ])
            ->add("medias", CollectionType::class, [
                "required" => false,
                "entry_type" => MediaType::class,
                'label' => "Medias",
                'allow_add' => true,
                'allow_delete' => true,
                "by_reference" => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Chapter::class,
        ]);
    }
}
