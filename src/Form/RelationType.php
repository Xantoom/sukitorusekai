<?php

namespace App\Form;

use App\Entity\Character;
use App\Entity\Relation;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RelationType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add("type", TextType::class, [
                "label" => "Relation type",
                "required" => false,
            ])
            ->add("relative", EntityType::class, [
                "label" => "Relative",
                "required" => false,
                "class" => Character::class,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Relation::class,
        ]);
    }

}