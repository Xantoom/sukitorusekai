<?php

namespace App\Form;

use App\Entity\Arc;
use App\Entity\Chapter;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ArcType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                "label" => "Title",
                "required" => true,
                'constraints' => [
                    new NotBlank(['message' => "The title must not be blank."]),
                ],
            ])
            ->add('romaji', TextType::class, [
                "label" => "Romaji Title",
                "required" => true,
                'constraints' => [
                    new NotBlank(['message' => "The romaji name must not be blank."]),
                ],
            ])
            ->add("chapters", EntityType::class, [
                "required" => false,
                "class" => Chapter::class,
                "label" => "Chapters",
                "multiple" => true,
                "data" => $options["data"]->getChapters(),
                'choice_label' => function (?Chapter $chapter) {
                    if(count($chapter->getEpisodes()) != 0) {
                        $numbers = [];
                        foreach($chapter->getEpisodes() as $episode) $numbers[] = $episode->getNumber();
                        return "Chapter " . $chapter->getNumber()  . " - Episode(s) " . implode("/", $numbers);
                    }
                    else return "Chapter " . $chapter->getNumber();
                },
            ])
            ->add("cover", MediaType::class, [
                "label" => "Cover Picture",
                "required" => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Arc::class,
        ]);
    }
}
