<?php

namespace App\Form;

use App\Entity\Chapter;
use App\Entity\Episode;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class EpisodeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                "required" => true,
                "label" => "Title",
                "constraints" => [
                    new NotBlank(["message" => "The title must not be blank."])
                ]
            ])
            ->add('romaji', TextType::class, [
                "required" => true,
                "label" => "Romaji Title",
                "constraints" => [
                    new NotBlank(["message" => "The romaji title must not be blank."])
                ]
            ])
            ->add('number', NumberType::class, [
                "required" => false,
                "label" => "Number",
            ])
            ->add('duration', NumberType::class, [
                "required" => false,
                "label" => "Duration",
            ])
            ->add('directors', TextType::class, [
                "required" => false,
                "label" => "Episode Director(s)",
                "mapped" => false
            ])
            ->add("storyboard", TextType::class, [
                "required" => false,
                "label" => "Storyboard",
                "mapped" => false
            ])
            ->add("animDirectors", TextType::class, [
                "required" => false,
                "label" => "Animation Director(s)",
                "mapped" => false
            ])
            ->add("description", CKEditorType::class, [
                "label" => "Description",
                "attr" => [
                    "rows" => 16
                ],
                "config" => [
                    "height" => 400,
                    "resize_enabled" => false
                ],
            ])
            ->add("releasedAt", DateType::class, [
                "label" => "Release Date",
                "required" => false
            ])
            ->add('medias', CollectionType::class, [
                "entry_type" => MediaType::class,
                'label' => "Medias",
                'allow_add' => true,
                'allow_delete' => true,
                "by_reference" => false
            ])
            ->add('chapters', EntityType::class, [
                "required" => false,
                "label" => "Chapters",
                "multiple" => true,
                "class" => Chapter::class,
                "data" => $options["data"]->getChapters(),
                'choice_label' => function (?Chapter $chapter) {return "Chapter " . $chapter->getNumber();},
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Episode::class,
        ]);
    }
}
