<?php

namespace App\Form;

use App\Entity\Ability;
use App\Entity\Chapter;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class AbilityType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, [
                "label" => "Name",
                "required" => true,
                'constraints' => [
                    new NotBlank(['message' => "The name must not be blank."]),
                ],
            ])
            ->add('romaji', TextType::class, [
                "label" => "Romaji Name",
                "required" => true,
                'constraints' => [
                    new NotBlank(['message' => "The romaji name must not be blank."]),
                ],
            ])
            ->add("summary", CKEditorType::class, [
                "label" => "Summary",
                "attr" => [
                    "rows" => 16
                ],
                "config" => [
                    "height" => 100,
                    "resize_enabled" => false
                ],
            ])
            ->add("description", CKEditorType::class, [
                "label" => "Detailed Description",
                "attr" => [
                    "rows" => 16
                ],
                "config" => [
                    "height" => 400,
                    "resize_enabled" => false
                ],
            ])
            ->add("spoilChapter", EntityType::class, [
                "class" => Chapter::class,
                "label" => "This post spoil the chapter:",
                "data" => $options["data"]->getSpoilChapter(),
                'choice_label' => function (?Chapter $chapter) {
                    if(!empty($chapter->getEpisodes())) {
                        $numbers = [];
                        foreach($chapter->getEpisodes() as $episode) $numbers[] = $episode->getNumber();
                        if(!empty($numbers)) return "Chapter " . $chapter->getNumber()  . " - Episode " . implode("/", $numbers);
                        return "Chapter " . $chapter->getNumber();
                    }
                    else return "Chapter " . $chapter->getNumber();
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Ability::class,
        ]);
    }
}
