<?php

namespace App\Form;

use App\Entity\Chapter;
use App\Entity\Volume;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class VolumeType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('title', TextType::class, [
                "label" => "Title",
                "required" => true,
                'constraints' => [
                    new NotBlank(['message' => "The title must not be blank."]),
                ],
            ])
            ->add('romaji', TextType::class, [
                "label" => "Title",
                "required" => true,
                'constraints' => [
                    new NotBlank(['message' => "The title must not be blank."]),
                ],
            ])
            ->add("cover", MediaType::class, [
                "label" => "Cover Picture",
                "required" => false,
                "data" => $options["data"]->getCover(),
            ])
            ->add("releasedAt", DateType::class, [
                "label" => "Release Date",
                "required" => false
            ])
            ->add("chapters", EntityType::class, [
                "required" => false,
                "label" => "Chapters",
                "multiple" => true,
                "class" => Chapter::class,
                "data" => $options["data"]->getChapters(),
                'choice_label' => function (?Chapter $chapter) {return "Chapter " . $chapter->getNumber();},
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Volume::class,
        ]);
    }

}